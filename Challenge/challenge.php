<html>
	<head>
		<title>

		</title>
		<link rel="stylesheet" href="../style/style.css"> 
		<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
		<link rel="stylesheet" href="../style/responsife.css">
		<link rel="stylesheet" href="../style/events.css"> 
		<link rel="stylesheet" href="../style/challenge.css">
		<link rel="stylesheet" href="../style/learning.css">

		<style>
		* {box-sizing: border-box}
		body {font-family:Calibri Light; sans-serif; margin:0}
		.mySlides {display: none}
		img {vertical-align: middle;}

		.slideshow-container {
		  max-width: 1800px;
		  position: relative;
		  margin: auto;
		}
		.prev, .next {
		  cursor: pointer;
		  position: absolute;
		  top: 50%;
		  width: auto;
		  padding: 16px;
		  margin-top: -22px;
		  color: white;
		  font-weight: bold;
		  font-size: 18px;
		  transition: 0.6s ease;
		  border-radius: 0 3px 3px 0;
		}
		.next {
		  right: 0;
		  border-radius: 3px 0 0 3px;
		}
		.prev:hover, .next:hover {
		  background-color: rgba(0,0,0,0.8);
		}
		.tais {
		  color: #f2f2f2;
		  font-size:40px;
		  padding: 0px 12px;
		  position: absolute;
		  top:500px;
		  bottom: 8px;
		  width: 100%;
		  text-align: center;
		}
		.text {
		  color: #f2f2f2;
		  font-size:30px;
		  padding: 8px 12px;
		  position: absolute;
		  bottom: 8px;
		  width: 100%;
		  text-align: center;
		}
		.numbertext {
		  color: #f2f2f2;
		  font-size: 40px;
		  padding: 36%;  100px;
		  position: absolute;
		  top: 0;
		}
		.dot {
		  cursor: pointer;
		  height: 15px;
		  width: 15px;
		  margin: 0 2px;
		  background-color: #bbb;
		  border-radius: 50%;
		  display: inline-block;
		  transition: background-color 0.6s ease;
		}

		.active, .dot:hover {
		  background-color: #717171;
		}
		.fade {
		  -webkit-animation-name: fade;
		  -webkit-animation-duration: 1.5s;
		  animation-name: fade;
		  animation-duration: 1.5s;
		}

		@-webkit-keyframes fade {
		  from {opacity: .4} 
		  to {opacity: 1}
		}

		@keyframes fade {
		  from {opacity: .4} 
		  to {opacity: 1}
		}
		@media only screen and (max-width: 600px) {
		  .prev, .next,.text {font-size: 11px}
		}
		@media screen and (max-width: 860px) {
		.tais {
		  font-size:30px;
		}
		.text {
		  font-size:20px;
		}
		}
		@media screen and (max-width: 770px) {
		.tais {
		  top:200px;
		  font-size:24px;
		}
		.text {
		  font-size:14px;
		}
		}
		@media screen and (max-width: 480px) {
		.tais {
		  font-size:24px;
		}
		.text {
		  font-size:14px;
		}
		}
		@media screen and (max-width: 400px) {
		.tais {
		  top:500px;
		}
		}
		@media screen and (max-width: 360px) {
		.tais {
		  top:500px;
		}
		}
		@media screen and (max-width: 320px) {
		.tais {
		  top:400px;
		}
		}
		</style>
	</head>
	<body>
		<header>
			<div class="hover">
				<div class="toggle">
					<div class="logi"><a href="../index.php"><img src="../img/k.png"></a></div>
					<i class="fas fa-bars menu"></i>	
				</div>
				<ul>
					<div class="logo"><a href="../index.php"><img src="../img/k.png"></a></div>
					<li><a href="../Learning/learning.php"><i class="fas fa-graduation-cap"></i>Learning</a></li>
					<li><a href="../Events/events.php"><i class="far fa-calendar-alt"></i></i>Events</a></li>
					<li class="dropdown">
						<a href="javascript:void(0)" class="dropbtn"><i class="fas fa-trophy"></i>Challenge</a>
						<div class="dropdown-content">
						  <a href="../Challenge/challenge.php">Masih Berlangsung</a>
						  <a href="#">Sudah Selesai</a>
						  <a href="#">Sedang Berlangsung</a>
						</div>
					</li>
					<li><a href="../aboutus/aboutus.php"><i class="fas fa-user"></i>About us</a></li>
				<div class="login">
					<li><a href="../login.php"><i class="fas fa-sign-in-alt" style="color:white;"></i>Login</a></li>
				</div>
			</div>
			</ul>
		</header>
			<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
				<script type="text/javascript">
					$(document).ready(function(){
						$('.menu').click(function(){
						$('ul').toggleClass('active');
						})
					})
				</script>
		<section>
			<div style="margin-top:4px;" class="slideshow-container">
				<div style="width:100%;"class="mySlides fade">
				   <img src="../images/buku.jpg" style="width:100%;height:92%;">
				   <div class="tais">Lihat Bakat Yang Kamu Miliki</div>
				  <div class="text">Dapatkan Pengalaman Belajar Lebih Baik</div>
				   
				</div>

				<div class="mySlides fade">
				  <img src="../images/buku.jpg" style="width:100%; height:94%;">
				   <div class="tais">Bangun Karir dan Jadilah Mahasiswa Profesional</div>
				  <div class="text">Belajar Langsung Dari Ahlinya</div>
				</div>

				<div class="mySlides fade">
				  <div class="numbertext"></div>
				  <img src="../images/le.jpg" style="width:100%;height:92%;">
				   <div class="tais">Lihat Bakat Yang Kamu Miliki</div>
				  <div class="text">Kembangkan Bakaymu dan bekerja di Industri yang kalian Inginkan </div>
				</div>

				<a class="prev" onclick="plusSlides(-1)">&#10094;</a>
				<a class="next" onclick="plusSlides(1)">&#10095;</a>

				</div>
				<br>
				<div style="text-align:center">
				  <span class="dot" onclick="currentSlide(1)"></span> 
				  <span class="dot" onclick="currentSlide(2)"></span> 
				  <span class="dot" onclick="currentSlide(3)"></span> 
				</div>
				<script>
				var slideIndex = 1;
				showSlides(slideIndex);

				function plusSlides(n) {
				  showSlides(slideIndex += n);
				}

				function currentSlide(n) {
				  showSlides(slideIndex = n);
				}

				function showSlides(n) {
				  var i;
				  var slides = document.getElementsByClassName("mySlides");
				  var dots = document.getElementsByClassName("dot");
				  if (n > slides.length) {slideIndex = 1}    
				  if (n < 1) {slideIndex = slides.length}
				  for (i = 0; i < slides.length; i++) {
					  slides[i].style.display = "none";  
				  }
				  for (i = 0; i < dots.length; i++) {
					  dots[i].className = dots[i].className.replace(" active", "");
				  }
				  slides[slideIndex-1].style.display = "block";  
				  dots[slideIndex-1].className += " active";
				}
				</script>
		</section>
		<section>
			<div class="contain">
				<div class="selesih">
				
				</div>
				<div class="terong">
					<div class="texts">
						<form method="GET">
							<input type="text" onkeyup="showname(this.value)"  style="width:100%; height:100%;"name="search" placeholder="    enter your challenge.....">
						</form>
					</div>
					<div class="cimahi">
						<select style="background:#37474F; color:white; height:29px;" onchange="showid(this.value)">
							<option value="0">&nbsp &nbsp &nbsp ----  Pilih Challenge  ----</option>
							<?php
							session_start();
								include("../config.php");
									$query = $connect->query("SELECT * FROM challengel");
										if ($query->num_rows !=0) {	
											while($data =$query->fetch_object()){
												echo "<option value='{$data->id_challenge}'>{$data->judulchallenge}</option>";
											}
										}
										else{
											while($data =$query->fetch_object()){
												$query = $connect->query("SELECT * FROM challengel");	
											}
										} 
							?>
						</select>
					</div>
				</div>
				<div id="combo" style="margin-left:0%;">
				<?php
						$query = $connect->query("SELECT * FROM challengel ");
						if ($query->num_rows !=0) {		
							while($data =$query->fetch_object()){		
				?>
				<div class="conten" style="height:auto;">
					<div class="webPemula" style="height:20%;">
						<div class="bagi">
							<div class="class">
								<div class="danusa">
									<p><?php echo $data-> judulchallenge;?></p>
								</div>
								<div class="danu">
									<div class="imga">
										<img src="../img/<?php echo $data->gambar_challenge;?>">
									</div>
									<div class="texi">
										<p><?php echo $data->deskripsi;?></p>
									</div>
								</div>
							</div>
							<div class="cla">
								
							</div>
							<div class="cl">
								<div class="danusa">
									<p>Fitur Fitur</p>
								</div>
								<div class="danu">
									<div class="texu">
										<p href=""><i class="fas fa-user"></i><?php echo $data->jumlah_siswa;?></p>
										<p href=""><i class="fas fa-gift"></i><?php echo $data-> xp;echo"xp &nbsp"; echo $data->poin;echo"poin &nbsp"; ?> </p>
										<p href=""><i class="fas fa-chess-queen"> </i><?php echo $data->tingkat_kelas; ?> </p>
										<p href=""><i class="fas fa-unlock-alt"></i> <?php echo $data-> status;?></p>
										<p href=""><i class="fas fa-dollar-sign"></i><?php echo $data->harga; ?></p>
									</div>
								</div>
							</div>
						</div>
						<div class="bagian">
						<li>
							<a href="../login.php">Mulai</a>
							</li>
						</div>					
					</div>
					<?php 
						} 
					} 
					?>
					<center>
					<div class="pagination">
					  <a href="#">&laquo;</a>
					  <a href="#">1</a>
					  <a class="active" href="#">2</a>
					  <a href="#">3</a>
					  <a href="#">4</a>
					  <a href="#">5</a>
					  <a href="#">6</a>
					  <a href="#">7</a>
					  <a href="#">8</a>
					  <a href="#">9</a>
					  <a href="#">10</a>
					  <a href="#">&raquo;</a>
					</div>
					</center>
				</div>
		</section>
		<footer>
			<div class="foote">
				<div class="latar">
					<div class="logoo">
						<div class="deskripsi">
							<h1>Koda Coach</h1>
						</div>
						<div class="deskripsii">
							<p>
							 sebuah aplikasi berbasis web yang memuiliki tujuan sebagai pelepor pendidikan di indonesia khususnya dalam bidang program. disusun dengan modul modul terpercaya yang dapat mempermudah proses pembelajaran.
							</p>
						</div>
					</div>
					<div class="logoa">
							<div class="samp">
							<a href="../Learning/learning.php"><i class="fas fa-graduation-cap"></i>Learning</a>
							</div>
							<a href="../Events/events.php"><i class="far fa-calendar-alt"></i>Events</a>
							<div class="samp">
							<a href="../Challenge/challenge.php"><i class="fas fa-trophy"></i>Challenge</a>
							</div>
							<div class="samp">
							<a href="../aboutus/aboutus.php"><i class="fas fa-user"></i>About Us</a>
							</div>
					</div>
					<div class="logon">
						<div class="clas">
							<div class="bund">
								<i class="fab fa-facebook-f"></i>
							</div>
							<div class="bunder">
								<i class="fab fa-twitter"></i>
							</div>
							<div class="bunder">
								<i class="fab fa-telegram-plane"></i>
							</div>
							<div class="bunder">
								<i class="fab fa-google-plus-g"></i>
							</div>
						</div>
						<div class="clak">
							<img src="../img/li.png">
							<img src="../img/b.png">
							<img src="../img/c.png">
							<img src="../img/f.png">
						</div>
					</div>
				</div>
				<div class="latarr">
				</div>
			</div>
		</footer>	
	</body>
</html>