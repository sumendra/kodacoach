var listTodo = document.getElementById("daftarTodo");

var request = new XMLHttpRequest();
request.open("GET", "js/data.json");

request.onload = function() {

  var response = this.responseText;

  if (response == "") {
    console.log("kosong");
  } else {
    var todos = JSON.parse(response);
    var renderHTML = "";

    for (i = 0; i < todos.length; i++) {
      var openTag = "<li id='" + todos[i].keterangan + "'>";
      var value = todos[i].keterangan;
      var button =
        "<button value='" +
        todos[i].keterangan +
        "' onclick='remove(this.value)'>Hapus</button>";
      var closeTag = "</li>";

      renderHTML += openTag + value + button + closeTag;
    }

    listTodo.innerHTML = renderHTML;
  }
};

request.send();

// ====================================================================

function remove(id) {

  var request = new XMLHttpRequest();
  request.open("GET", "remove.php?key=" + id);

  request.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      var tobeRemoved = document.getElementById(id);
      tobeRemoved.remove();
      console.log("berhasil di hapus");
    }
  };

  request.send();
}

// ====================================================================

var addToDo = document.getElementById("addToDo");

addToDo.addEventListener("click", addNewItem);

function addNewItem() {

  var todo = document.getElementById("todo").value;
  
  var objTodo = {
    keterangan: todo
  };

  var jsonTodo = JSON.stringify(objTodo);

  var request = new XMLHttpRequest();
  request.open("POST", "insert.php", true);

  request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

  request.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      console.log(this.responseText);
    }
  };

  request.send(encodeURI("todo=" + jsonTodo));

  var openTag = "<li id='" + todo + "'>";
  var value = todo;
  var button =
    "<button value='" + todo + "' onclick='remove(this.value)'>Hapus</button>";
  var closeTag = "</li>";

  var renderTodo = openTag + value + button + closeTag;
  listTodo.insertAdjacentHTML("afterBegin", renderTodo);
  
}
