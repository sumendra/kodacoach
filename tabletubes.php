<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Admin | Koda Coach</title>
     <link href="admin/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="admin/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
    <link href="admin/dist/css/sb-admin-2.css" rel="stylesheet">
    <link href="admin/vendor/morrisjs/morris.css" rel="stylesheet">
    <link href="admin/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
</head>
<body>
<div id="wrapper">
	 <nav class="navbar navbar-inverse navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php" style="color: white;">Koda Coach</a>
            </div>
			<ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-envelope fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-messages">
					<?php include("connect.php");
				
					$bela=mysqli_query ($connect, "select * from user ");
					while($rama=mysqli_fetch_array($bela)){?>
                       
                        <li class="divider"></li>
                        <li>
                            <a href=" ">
                                <div>
                                    <strong><?php echo $rama['username'];?></strong>
                                    <span class="pull-right text-muted">
                                        <em>17 m yang lalu</em>
                                    </span>
                                </div>
                                <div><?php echo $rama['keterangan'];?></div>
                            </a>
                        </li>
					<?php } ?>
                    </ul>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-tasks fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-tasks">
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <p>
                                        <strong>Learing</strong>
                                        <span class="pull-right text-muted">80% Complete</span>
                                    </p>
                                    <div class="progress progress-striped active">
                                        <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                                            <span class="sr-only">80% Complete (danger)</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <p>
                                        <strong>Event</strong>
                                        <span class="pull-right text-muted">20% Complete</span>
                                    </p>
                                    <div class="progress progress-striped active">
                                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                                            <span class="sr-only">20% Complete</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <p>
                                        <strong>Challenge</strong>
                                        <span class="pull-right text-muted">60% Complete</span>
                                    </p>
                                    <div class="progress progress-striped active">
                                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
                                            <span class="sr-only">60% Complete (warning)</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        
                        <li class="divider"></li>
                        <li>
                            <a class="text-center" href="#">
                                <strong>See All Tasks</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                    </ul>
                    <!-- /.dropdown-tasks -->
                </li>
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-bell fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-alerts">
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
								<?php $news=mysqli_query($connect,"select waktu,max(id_member) as id_member from member order by id_member asc"); while($a=mysqli_fetch_array($news)){?>
                                    <i class="fa fa-twitter fa-fw"></i><?php echo $a['id_member'];?> Member
								<?php } ?>
								<?php  
									$rama= mysqli_query ($connect ,  "select waktu from member where id_member=(select max(id_member) from member)"); 
									$siwa=mysqli_fetch_array($rama);
									$waktu=$siwa['waktu'];
									include 'time_since.php';
									$lalu=time_since(strtotime ($waktu) );
								?>
                                    <span class="pull-right text-muted small"><?php echo $lalu ?></span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a class="text-center" href="#">
                                <strong>See All Alerts</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="logout.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                </li>
            </ul>
            
	            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                            </div>
                        <li>
                            <a href="isiadmin.php"><i class="fas fa-columns"></i> Dashboard</a>
                        </li>
						<li>
                            <a  href="tables.php"><i style="padding-right:6px;" class="fas fa-user"></i></i> Table Member<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
								<li>
                                    <a href="tables.php">Member</a>
                                </li>
                                <li>
                                    <a href="tablelearning.php">Learning</a>
                                </li>
                                <li>
                                    <a href="tableevents.php">Events</a>
                                </li>
                                <li>
                                    <a href="tablechallenge.php">Challenge</a>
                                </li>
								 <li>
                                    <a href="tablequis.php">Quis</a>
                                </li>
                                <li>
                                    <a href="tabletubes.php">Tubes</a>
                                </li>
								 <li>
                                    <a href="tablebayarevents.php">Bayar Events</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#"><i style="padding-right:6px;" class="fas fa-book"></i></i> Table Materi<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="tableisilearning.php">Learing</a>
                                </li>
                                <li>
                                    <a href="tableisievents.php">Events</a>
                                </li>
                                <li>
                                    <a href="tableisichallenge.php">Challenge</a>
                                </li>
								 <li>
                                    <a href="tableisiquis.php">Quis</a>
                                </li>
                                <li>
                                    <a href="tableisitubes.php">Tubes</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Tables</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Table Member Koda Coach
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr style="background:#1565C0;color:white;">
                                        <th style="font-size:10px;font-family:Calibri Light ;">id_tubes</th>
										<th style="font-size:10px;font-family:Calibri Light;">id_member</th>
										<th style="font-size:10px;font-family:Calibri Light;">id_learning</th>
										<th style="font-size:10px;font-family:Calibri Light;">Judul Uploadtan</th>
                                        <th style="font-size:10px;font-family:Calibri Light;">deskripsi upload</th>
										<th style="font-size:10px;font-family:Calibri Light;">Tipe File</th>
										<th style="font-size:10px;font-family:Calibri Light;">Ukuran_File</th>
										<th style="font-size:10px;font-family:Calibri Light;">Judul_Sertifikat</th>
										<th style="font-size:10px;font-family:Calibri Light;">aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
								<?php 
									$rama= mysqli_query ($connect, "select * from mulai_tubes  ");
									while($siwa=mysqli_fetch_array($rama)){
								?>
                                    <tr class="odd gradeX">
                                        <td style="font-size:10px;font-family:Calibri Light;"><?php echo $siwa['id_tubes']; ?></td>
                                        <td style="font-size:10px;font-family:Calibri Light;" ><?php echo $siwa['id_member']; ?></td>
										<td style="font-size:10px;font-family:Calibri Light;"><?php echo $siwa['id_learning'];?></td>
										 <td style="font-size:10px;font-family:Calibri Light;"><?php echo $siwa['judul_hasil']; ?></td>
                                        <td style="font-size:10px;font-family:Calibri Light;" ><?php echo $siwa['deskripsi_hasil']; ?></td>
										 <td style="font-size:10px;font-family:Calibri Light;" ><?php echo $siwa['tipe_file']; ?></td>
										  <td style="font-size:10px;font-family:Calibri Light;" ><?php echo $siwa['ukuran_file']; ?></td>
										   <td style="font-size:10px;font-family:Calibri Light;" ><?php echo $siwa['judul_sertifikat']; ?></td>
                                        <td style="width:34%;"> 
										<div class="edit" style="width:90px; height:36px;float:left;margin-right:10px; border-radius:5px;">
										<ul style="margin-top:10%;">
										<a style="color:white;text-decoration:none;"; href="updatetubes.php?id_tubes=<?php echo $siwa['id_tubes']; ?>"><i style="color:orange;" class="fas fa-edit"></i></a>
										</ul>
										</div>
										<div class="edit" style="width:100px; height:36px;float:left;border-radius:5px; margin-right:14px;">
										<ul style="margin-top:10%;">
										<a style="color:white;text-decoration:none;"; href="hapustubes.php?id_tubes=<?php echo $siwa['id_tubes']; ?>"><i style="color:red" class="fas fa-trash-alt"></i></a>
										</ul>
										</div>
										<div class="edit" style="width:69px; height:27px;float:left;margin-top:9px; border-radius:5px;">
										<?php
										$id_tubes=$siwa['id_tubes'];
											$connect=mysqli_connect('localhost','root','sumendra','database');
											$sql=mysqli_query($connect,"SELECT * FROM mulai_tubes where id_tubes='$id_tubes' ");
											if($sql){
												$data=mysqli_fetch_array($sql);
													echo '
														<a href="'.$data['file'].'"><i class="fas fa-cloud-download-alt"></i></a>
													';
												
											}
											?>
										</div>
										<div class="edit" style="width:100px; height:36px;float:left;border-radius:5px; margin-right:0px;">
										<ul style="margin-top:10%;">
										<a style="color:white;text-decoration:none;padding:0;"; href="uploadsertifikat.php?id_tubes=<?php echo $siwa['id_tubes']; ?>"><i style="color:#37474F;" class="fas fa-cloud-upload-alt"></i></a>
										</ul>
										</div>
										</td>
                                    </tr>
									<?php }?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
   
    <script src="admin/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="admin/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="admin/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="admin/vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="admin/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="admin/vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="admin/dist/js/sb-admin-2.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
    </script>

</body>

</php>
