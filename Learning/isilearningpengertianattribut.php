<html>
	<head>
		<title>

		</title>
		<link rel="stylesheet" href="../style/style.css"> 
		<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
		<link rel="stylesheet" href="../style/responsife.css">
		<link rel="stylesheet" href="../style/events.css"> 
		<link rel="stylesheet" href="../style/learning.css"> 
		<link rel="stylesheet" href="../style/isilearning.css"> 
		<link rel="stylesheet" href="../style/indexlogin.css"> 
		<link rel="stylesheet" href="../style/isilearningpengertianattribut.css"> 
		<link rel="stylesheet" href="../style/responsifeattribut.css"> 
	</head>
	<body>
	
		<header>
		
			<div class="hover">
				<div class="toggle">
					<div class="logi"><a href="../indexlogin.php"><img src="../img/k.png"></a></div>
					<i class="fas fa-bars menu"></i>	
				</div>
			<ul>
				  <div class="logo"><a href="../indexlogin.php"><img src="../img/k.png"></a></div>
				  <li><a href="../Learning/learninglogin.php"><i class="fas fa-graduation-cap"></i>Learning</a></li>
				  <li><a href="../Events/eventslogin.php"><i class="far fa-calendar-alt"></i></i>Events</a></li>
				  <li class="dropdown">
					<a href="javascript:void(0)" class="dropbtn"><i class="fas fa-trophy"></i>Challenge</a>
					<div class="dropdown-content">
					  <a href="../Challeng/challengelogin.php">Masih Berlangsung</a>
					  <a href="#">Sudah Selesai</a>
					  <a href="#">Sedang Berlangsung</a>
					</div>
				  </li>
				  <li><a href="../reward.php"><i class="fas fa-gift"></i>Reward</a></li>
			
				<div class="Login/login">
					 <div class="pete"><a href="../Profil/profil.php"><img src="../img/user.png"></a></div>
				</div>
			</div>
			</ul>
		</header>
		<script src="https://code.jquery.com/jquery-3.3.1.js"
		></script>
		<script type="text/javascript">
			$(document).ready(function(){
				$('.menu').click(function(){
				$('ul').toggleClass('active');
				})
			})
		</script>
		<section>
				<div class="container">
					<div class="content">
					<div class="zu">
					</div>
					<div class="texs">
						<h2>Web Development Beginner</h2>
					</div>
				</div>
		</section>
		<section>
			<div class="word">
				<div style="width:24.7%; height:100%;" class="siji">
				<center>
					<i class="fas fa-clock"></i>
					<p>2 Bulan</p>
				</center>
				</div>
				<div style="width:0.4%; height:100%;" class="papat">
					
				</div>
				<div style="width:24.7%; height:100%;" class="siji">
				<center>
					<i class="fas fa-home"></i>
					<p>SAMSUNG</p>
				</center>
				</div>
				<div style="width:0.4%; height:100%;" class="papat">
				
				</div>
				<div style="width:24.7%; height:100%;" class="siji">
				<center>
					<i class="fas fa-chess-king"></i>
					<p>Beginner</p>
				</center>
				</div>
				<div style="width:0.4%; height:100%;" class="papat">
				
				</div>
				<div style="width:24.7%; height:100%;" class="siji">
				<center>
					<i class="fas fa-trophy"></i>
					<p>1000 xp dan 0 poin</p>
				</center>
				</div>
			</div>
		</section>
		<section>
			<div class="isilearning">
				<div class="ju">
					<div class="ta">
						<h4>Selamat Datang </h4> 
						<h4>di Tutorial Html dan Css </h4>
					</div>
					<div class="j">
						<a href="../Learning/isilearningpengertianhtml.php"><i class="fas fa-play-circle"></i> Pengenalan awal html dan css dasar </a>
						<a href="../Learning/isilearningpengertianattribut.php"><i class="fas fa-book"></i>mengenal html element dan attribut</a>
						<a href=""><i class="fas fa-book"></i> mengenal css warna dan background</a>
						<a href=""><i class="fas fa-play-circle"></i> mengenal html pod dan paragraf</a>
						<a href=""><i class="fas fa-book"></i> mengenal css width dan height  </a>
						<a href=""><i class="fas fa-book"></i> mengenal html formatif dan kutipan</a>
						<a href=""><i class="fas fa-book"></i> mengenal css text dan font</a>
						<a href=""><i class="fas fa-play-circle"></i> mengenal html komentar dan warna</a>
						<a href=""><i class="fas fa-book"></i> mengenal Css border, margin , dan.... </a>
				
						<a href="../Learning/isilearningquisperbedaanpadding.php"><i class="fas fa-star"></i> apa perbedaan antara padding dan.... </a>
							
						<a href=""><i class="fas fa-book"></i> mengenal html image dan html table</a>
						<a href=""><i class="fas fa-play-circle"></i> mengenal html form </a>
						<a href=""><i class="fas fa-book"></i> mengenal css float dan overflow</a>
						<a href=""><i class="fas fa-book"></i> mengenal html layout </a>
							
						<a href=""><i class="fas fa-play-circle"></i> mengenal css opacity dan navigation bar</a>
						<a href="../Learning/isilearningpengertiancanvas.php"><i class="fas fa-book"></i> mengenal html canvas dan map </a>
						<a href="../Learning/isilearningpengertiandropdown.php"><i class="fas fa-play-circle"></i> mengenal css dropdown dan media query</a>
						<a href="../Learning/tubes.php"><i class="fas fa-trophy"></i> Buatlah sebuah website sesuai tema .... </a>
					</div>
				</div>
				<div class="h">
						<h1>Pengertian Html Attribut dan Element </h1>
						<div class="tess">
						<p>&nbsp &nbsp &nbsp &nbsp Elemen, dan atribut pada HTML sebenarnya saling berkaitan,
						hanya saja disini atribut memiliki tugas khusus untuk memberikan informasi atau sifat 
						tambahan yang akan diberikan kepada tag dan elemen yang mengandung atribut tersebut. 
						</p><br>
						
						<h4>cara mendefinisikan attribbut</h4><br>
						
						<p>Penulisan Atribut selalu diletakan dalam tag pembuka  setelah nama tag, sebagai
						contoh: name="value"  Dari tag dan atribut tersebut yang dimaksud dengan
						atribut adalah name="value". Informasi ini berupa instruksi tambahan yang tersusun dari
						dua bagian yaitu atribut Name dan atribut Value dalam penulisannya atribut value harus
						ditulis diantara tanda kutip ganda atau tanda kutip tunggal.</p><br>
						
						<h4>cara mendefinisikan element</h4><br>
						<p>Element adalah isi dari tag yang berada diantara tag pembuka dan tag penutup, termasuk tag 
						itu sendiri dan atribut yang dimilikinya (jika ada). Sebagai contoh perhatikan kode HTML berikut:</p><br>
						<p> ini adalah paragraf </p><br>
						<p>ini adalah paragraf merupakan element dan p merupakan tag paragraf</p>
						
						</div>
						
				</div>
				
			</div>		
		</section>
		<footer>
			<div class="foote">
				<div class="latar">
					<div class="logoo">
						<div class="deskripsi">
							<h1>Koda Coach</h1>
						</div>
						<div class="deskripsii">
							<p>
							 sebuah aplikasi berbasis web yang memuiliki tujuan sebagai pelepor pendidikan di indonesia khususnya dalam bidang program. disusun dengan modul modul terpercaya yang dapat mempermudah proses pembelajaran.
							</p>
						</div>
					</div>
					<div class="logoa">
							<div class="samp">
							<a href="../Learning/learninglogin.php"><i class="fas fa-graduation-cap"></i>Learning</a>
							</div>
							<div class="samp">
							<a href="../Events/eventslogin.php"><i class="far fa-calendar-alt"></i>Events</a>
							</div>
							<div class="samp">
							<a href="../Challenge/challengeloggin.php"><i class="fas fa-trophy"></i>Challenge</a>
							</div>
							<div class="samp">
							<a href="../aboutus/aboutuslogin.php"><i class="fas fa-user"></i>About Us</a>
							</div>
					</div>
					<div class="logon">
						<div class="clas">
							<div class="bund">
								<i class="fab fa-facebook-f"></i>
							</div>
							<div class="bunder">
								<i class="fab fa-twitter"></i>
							</div>
							<div class="bunder">
								<i class="fab fa-telegram-plane"></i>
							</div>
							<div class="bunder">
								<i class="fab fa-google-plus-g"></i>
							</div>
						</div>
						<div class="clak">
							<img src=".../img/li.png">
							<img src="../img/b.png">
							<img src="../img/c.png">
							<img src="../img/f.png">
						</div>
					</div>
				</div>
				<div class="latarr">
				</div>
			</div>
		</footer>	
	</body>
</html>