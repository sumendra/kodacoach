<html>
	<head>
		<title>

		</title>
		<link rel="stylesheet" href="../style/style.css"> 
		<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
		<link rel="stylesheet" href="../style/responsife.css">
		<link rel="stylesheet" href="../style/events.css"> 
		<link rel="stylesheet" href="../style/learning.css"> 
		<link rel="stylesheet" href="../style/isilearning.css"> 
		<link rel="stylesheet" href="../style/indexlogin.css">
		<link rel="stylesheet" href="../style/isilearningpengertianhtml.css"> 
		<link rel="stylesheet" href="../style/isilearningpengertiandropdown.css"> 
		<link rel="stylesheet" href="../style/responsifedropdown.css"> 
	</head>
	<body>
	
		<header>
		
			<div class="hover">
				<div class="toggle">
					<div class="logi"><a href="../indexlogin.php"><img src="../img/k.png"></a></div>
					<i class="fas fa-bars menu"></i>	
				</div>
			<ul>
				  <div class="logo"><a href="../indexlogin.php"><img src="../img/k.png"></a></div>
				  <li><a href="../Learning/learninglogin.php"><i class="fas fa-graduation-cap"></i>Learning</a></li>
				  <li><a href="../Events/eventslogin.php"><i class="far fa-calendar-alt"></i></i>Events</a></li>
				  <li class="dropdown">
					<a href="javascript:void(0)" class="dropbtn"><i class="fas fa-trophy"></i>Challenge</a>
					<div class="dropdown-content">
					  <a href="../Challenge/challengelogin.php">Masih Berlangsung</a>
					  <a href="#">Sudah Selesai</a>
					  <a href="#">Sedang Berlangsung</a>
					</div>
				  </li>
				 <li><a href="../reward.php"><i class="fas fa-gift"></i>Reward</a></li>
			
				<div class="login">
					 <div class="pete"><a href="../Profil/profil.php"><img src="../img/user.png"></a></div>
				</div>
			</div>
			</ul>
		</header>
		<script src="https://code.jquery.com/jquery-3.3.1.js"
		></script>
		<script type="text/javascript">
			$(document).ready(function(){
				$('.menu').click(function(){
				$('ul').toggleClass('active');
				})
			})
		</script>
		<section>
				<div class="container">
					<div class="content">
					<div class="zu">
					</div>
					<div class="texs">
						<h2>Web Development Beginner</h2>
					</div>
				</div>
		</section>
		<section>
			<div class="word">
				<div style="width:24.7%; height:100%;" class="siji">
				<center>
					<i class="fas fa-clock"></i>
					<p>2 Bulan</p>
				</center>
				</div>
				<div style="width:0.4%; height:100%;" class="papat">
					
				</div>
				<div style="width:24.7%; height:100%;" class="siji">
				<center>
					<i class="fas fa-home"></i>
					<p>SAMSUNG</p>
				</center>
				</div>
				<div style="width:0.4%; height:100%;" class="papat">
				
				</div>
				<div style="width:24.7%; height:100%;" class="siji">
				<center>
					<i class="fas fa-chess-king"></i>
					<p>Beginner</p>
				</center>
				</div>
				<div style="width:0.4%; height:100%;" class="papat">
				
				</div>
				<div style="width:24.7%; height:100%;" class="siji">
				<center>
					<i class="fas fa-trophy"></i>
					<p>1000 xp dan 0 poin</p>
				</center>
				</div>
			</div>
		</section>
		<section>
			<div class="isilearning">
				<div class="ju">
					<div class="ta">
						<h4>Selamat Datang </h4> 
						<h4>di Tutorial Html dan Css </h4>
					</div>
					<div class="j">
						<a href="../Learning/isilearningpengertianhtml.php"><i class="fas fa-play-circle"></i> Pengenalan awal html dan css dasar </a>
						<a href="../Learning/isilearningpengertianattribut.php"><i class="fas fa-book"></i>mengenal html element dan attribut</a>
						<a href=""><i class="fas fa-book"></i> mengenal css warna dan background</a>
						<a href=""><i class="fas fa-play-circle"></i> mengenal html pod dan paragraf</a>
						<a href=""><i class="fas fa-book"></i> mengenal css width dan height  </a>
						<a href=""><i class="fas fa-book"></i> mengenal html formatif dan kutipan</a>
						<a href=""><i class="fas fa-book"></i> mengenal css text dan font</a>
						<a href=""><i class="fas fa-play-circle"></i> mengenal html komentar dan warna</a>
						<a href=""><i class="fas fa-book"></i> mengenal Css border, margin , dan.... </a>
				
						<a href="../Learning/isilearningquisperbedaanpadding.php"><i class="fas fa-star"></i> apa perbedaan antara padding dan.... </a>
							
						<a href=""><i class="fas fa-book"></i> mengenal html image dan html table</a>
						<a href=""><i class="fas fa-play-circle"></i> mengenal html form </a>
						<a href=""><i class="fas fa-book"></i> mengenal css float dan overflow</a>
						<a href=""><i class="fas fa-book"></i> mengenal html layout </a>
							
						<a href=""><i class="fas fa-play-circle"></i> mengenal css opacity dan navigation bar</a>
						<a href="../Learning/isilearningpengertiancanvas.php"><i class="fas fa-book"></i> mengenal html canvas dan map </a>
						<a href="../Learning/isilearningpengertiandropdown.php"><i class="fas fa-play-circle"></i> mengenal css dropdown dan media query</a>
						<a href="../Learning/tubes.php"><i class="fas fa-trophy"></i> Buatlah sebuah website sesuai tema .... </a>
					</div>
				</div>
				<div class="h">
						<h1>Css Dropdown dan Media Query </h1>
						<img id="myImg" src="../img/taghtml.png" style="width:50%; height:10%; margin-left:24%; alt="Tag Tag sebelum memulai html" >
						<div id="myModal" class="modal">
						  <span class="close">×</span>
						  <img class="modal-content" id="img01">
						  <div id="caption"></div>
						</div>
						<script>
						var modal = document.getElementById('myModal');
						var img = document.getElementById('myImg');
						var modalImg = document.getElementById("img01");
						var captionText = document.getElementById("caption");
						img.onclick = function(){
							modal.style.display = "block";
							modalImg.src = this.src;
							captionText.innerHTML = this.alt;
						}
						var span = document.getElementsByClassName("close")[0];
						span.onclick = function() { 
							modal.style.display = "none";
						}
						</script>
						<div class="tess">
						<p>&nbsp &nbsp &nbsp &nbsp  pada modul ini kami akan membahas tentang dropdown dan media query pertama tama kami akan menjelaskan apa itu dropdown dan mediaquery. .</p><br>
						
						<h4>A. pengertian dropdown </h4><br>
						
						<p>&nbsp &nbsp &nbsp &nbsp dropdown merupakan suatu variasi menu dalam html di kalian membuat suatu menu ke bawah yang tidak terlihat jika tidak di sentuh oleh kusor. dropdown biasanya digunakan untuk menu yang memiliki jenis yang sama namun dalam segi fungsi berbeda. contohnya saat kalian membuat suatu menu transaksi di mana di dalam transaksi di bagi menjadi transaksi yang sedang berjalan, belum berjalan dan sudah selsai 
						</p><br>
						
						<h4>contoh penerapannya</h4><br>
						
						
						<p>keterangan tag </p><br>
						<p>HtmI dan  /HtmI  merupakan tag pembuka dan penutup dari html </p>
						<p>Head dan/Head  merupakan tag pembuka dan penutup dari head </p>
						<p>title dan /title  merupakan tag pembuka dan penutup totle </p>
						<p>Body dan /Body -merupakan tag pembuka dan penutup dari body </p><br>
						
						<h4>selanjutnya ikuti gambar di bawah ini</h4><br>
						
						
						<img id="myImg" src="../img/container.png" alt="Tag Tag sebelum memulai html"><br>
						<div id="myModal" class="modal">
						  <span class="close">×</span>
						  <img class="modal-content" id="img01">
						  <div id="caption"></div>
						</div><br>
						<script>
						
						var modal = document.getElementById('myModal');
						var img = document.getElementById('myImg');
						var modalImg = document.getElementById("img01");
						var captionText = document.getElementById("caption");
						img.onclick = function(){
							modal.style.display = "block";
							modalImg.src = this.src;
							captionText.innerHTML = this.alt;
						}
						var span = document.getElementsByClassName("close")[0];
						span.onclick = function() { 
							modal.style.display = "none";
						}
						</script>
												
						<p><divv class="containerr"> berfungsi untuk membuat wadah dari dropdown yang kita buat </p>
						<p><divv class='contentt'> merupakan button yang nantinya dapat kalian klik</p>
						<p><divv class='text'>untuk membuat data dari dropdown</p><br>
						
						
						<h4>selanjutnya ikuti gambar di bawah ini  </h4><br>
						
						<img id="myImg" src="../img/dropdown.png" sty alt="css dropdown"><br>
						<div id="myModal" class="modal">
						  <span class="close">×</span>
						  <img class="modal-content" id="img01">
						  <div id="caption"></div>
						</div><br>
						<script>
						var modal = document.getElementById('myModal');
						var img = document.getElementById('myImg');
						var modalImg = document.getElementById("img01");
						var captionText = document.getElementById("caption");
						img.onclick = function(){
							modal.style.display = "block";
							modalImg.src = this.src;
							captionText.innerHTML = this.alt;
						}
						var span = document.getElementsByClassName("close")[0];
						span.onclick = function() { 
							modal.style.display = "none";
						}
						</script>
						<li>background:untuk memberi warna background pada div</li>
						<li>color: untuk memberi warna pada tulisan </li>
						<li>padding: untuk memberi jarak antara content dan attribut </li>
						<li>border: untuk menambahkan garis pinggir pada div</li>
						<li>display: berfungsi untuk membuat tampilan suatu div sesuai keinginan kita</li>
						<li>position:berfungsi mengatur position dari suatu element</li>
						<li>min-width:berfungsi untuk mengatur min lebar dari sutu element</li>
						<li>z-index: berfungsi untuk membuat data bertumpuk </li>
						<li>text-decoration:berfungsi untuk mebuat decoration dari suatu web</li></p><br>
						
						<h4>B. pengertian media query  </h4><br>
						
						<p>Media query adalah suatu tag css yang berfungsi mengatur tingkat keresponsifan suatu website 
						</p>
						<p>media query di bagi menjadi 2 yaitu : </p><br>
						
						<p>1. max-width </p><br>
						<p>max-width merupakan suatu  pengaturan  dimana saat suatu tampilan  memiliki lebar masimum sesuai dengan media query yang di tentukan maka akan berubah sesuai dengan css yang terdapat pada media query tersebut  </p><br>
						
						<p>2. min-width </p><br>
						<p>min-width merupakan suatu  pengaturan dimana saat   suatu tampilan  memiliki lebar minimun yang sesuai dengan media query yang telah di tentukan maka akan berubah sesuai css yang terdapat dalam media query tersebut  </p><br>
						
						<h4>contoh penerapnanya </h4><br>
						
						<p>@media screen and(max-width:700){</p>
						<p>&nbsp &nbsp &nbsp &nbsp.text a {</p>
						<p>&nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp color:red; </p>
						<p>&nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp padding:10px; </p>
						<p>&nbsp &nbsp &nbsp &nbsp }</p>
						<p>&nbsp &nbsp &nbsp &nbsp } </p>
						
						<p>keterangan  </p><r>
						<p>dari kodingan di atas fungsi dari min-width:700 adalah membuat perubahan pada website saat ukuran layar minimal 700. perubahannnya terjadi pada div text bagian a dimana akan berubah warna tulisan menjadi merah dan lebar kiri, kanan, atas , bawah menjadi 10 px  </p><br>
						</div>
				</div>
				
			</div>		
		</section>
		<footer>
			<div class="foote">
				<div class="latar">
					<div class="logoo">
						<div class="deskripsi">
							<h1>Koda Coach</h1>
						</div>
						<div class="deskripsii">
							<p>
							 sebuah aplikasi berbasis web yang memuiliki tujuan sebagai pelepor pendidikan di indonesia khususnya dalam bidang program. disusun dengan modul modul terpercaya yang dapat mempermudah proses pembelajaran.
							</p>
						</div>
					</div>
					<div class="logoa">
							<div class="samp">
							<a href="../Learning/learninglogin.php"><i class="fas fa-graduation-cap"></i>Learning</a>
							</div>
							<div class="samp">
							<a href="../Events/eventslogin.php"><i class="far fa-calendar-alt"></i>Events</a>
							</div>
							<div class="samp">
							<a href="../Challenge/challengeloggin.php"><i class="fas fa-trophy"></i>Challenge</a>
							</div>
							<div class="samp">
							<a href="../aboutus/aboutuslogin.php"><i class="fas fa-user"></i>About Us</a>
							</div>
					</div>
					<div class="logon">
						<div class="clas">
							<div class="bund">
								<i class="fab fa-facebook-f"></i>
							</div>
							<div class="bunder">
								<i class="fab fa-twitter"></i>
							</div>
							<div class="bunder">
								<i class="fab fa-telegram-plane"></i>
							</div>
							<div class="bunder">
								<i class="fab fa-google-plus-g"></i>
							</div>
						</div>
						<div class="clak">
							<img src="../img/li.png">
							<img src="../img/b.png">
							<img src="../img/c.png">
							<img src="../img/f.png">
						</div>
					</div>
				</div>
				<div class="latarr">
				</div>
			</div>
		</footer>	
	</body>
</html>