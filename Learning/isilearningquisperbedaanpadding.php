<?php 
session_start();
?>
<html>
	<head>
		<title>

		</title>
		<link rel="stylesheet" href="../style/style.css"> 
		<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
		<link rel="stylesheet" href="../style/responsife.css">
		<link rel="stylesheet" href="../style/events.css"> 
		<link rel="stylesheet" href="../style/learning.css"> 
		<link rel="stylesheet" href="../style/isilearning.css"> 
		<link rel="stylesheet" href="../style/indexlogin.css">
		<link rel="stylesheet" href="../style/isilearningpengertianhtml.css"> 
		<link rel="stylesheet" href="../style/isilearningpengertiandropdown.css"> 
		<link rel="stylesheet" href="../style/isilearningquisperbedaanpadding.css"> 
		<link rel="stylesheet" href="../style/responsifepadding.css"> 
	</head>
	<body>
	
		<header>
		
			<div class="hover">
				<div class="toggle">
					<div class="logi"><a href="../indexlogin.php"><img src="../img/k.png"></a></div>
					<i class="fas fa-bars menu"></i>	
				</div>
			<ul>
				  <div class="logo"><a href="../indexlogin.php"><img src="../img/k.png"></a></div>
				  <li><a href="../Learning/learninglogin.php"><i class="fas fa-graduation-cap"></i>Learning</a></li>
				  <li><a href="../Events/eventslogin.php"><i class="far fa-calendar-alt"></i></i>Events</a></li>
				  <li class="dropdown">
					<a href="javascript:void(0)" class="dropbtn"><i class="fas fa-trophy"></i>Challenge</a>
					<div class="dropdown-content">
					  <a href="../Challenge/challengelogin.php">Masih Berlangsung</a>
					  <a href="#">Sudah Selesai</a>
					  <a href="#">Sedang Berlangsung</a>
					</div>
				  </li>
				  <li><a href="../reward.php"><i class="fas fa-gift"></i>Reward</a></li>
			
				<div class="login">
					 <div class="pete"><a href="../Profil/profil.php"><img src="../img/user.png"></a></div>
				</div>
			</div>
			</ul>
		</header>
		<script src="https://code.jquery.com/jquery-3.3.1.js"
		></script>
		<script type="text/javascript">
			$(document).ready(function(){
				$('.menu').click(function(){
				$('ul').toggleClass('active');
				})
			})
		</script>
		<section>
				<div class="container">
					<div class="content">
					<div class="zu">
					</div>
					<div class="texs">
						<h2>Web Development Beginner</h2>
					</div>
				</div>
		</section>
		<section>
			<div class="word">
				<div style="width:24.7%; height:100%;" class="siji">
				<center>
					<i class="fas fa-clock"></i>
					<p>2 Bulan</p>
				</center>
				</div>
				<div style="width:0.4%; height:100%;" class="papat">
					
				</div>
				<div style="width:24.7%; height:100%;" class="siji">
				<center>
					<i class="fas fa-home"></i>
					<p>SAMSUNG</p>
				</center>
				</div>
				<div style="width:0.4%; height:100%;" class="papat">
				
				</div>
				<div style="width:24.7%; height:100%;" class="siji">
				<center>
					<i class="fas fa-chess-king"></i>
					<p>Beginner</p>
				</center>
				</div>
				<div style="width:0.4%; height:100%;" class="papat">
				
				</div>
				<div style="width:24.7%; height:100%;" class="siji">
				<center>
					<i class="fas fa-trophy"></i>
					<p>1000 xp dan 0 poin</p>
				</center>
				</div>
			</div>
		</section>
		<section>
			<div class="isilearning">
				<div class="ju">
					<div class="ta">
						<h4>Selamat Datang </h4> 
						<h4>di Tutorial Html dan Css </h4>
					</div>
					<div class="j">
						<a href="../Learning/isilearningpengertianhtml.php"><i class="fas fa-play-circle"></i> Pengenalan awal html dan css dasar </a>
						<a href="../Learning/isilearningpengertianattribut.php"><i class="fas fa-book"></i>mengenal html element dan attribut</a>
						<a href=""><i class="fas fa-book"></i> mengenal css warna dan background</a>
						<a href=""><i class="fas fa-play-circle"></i> mengenal html pod dan paragraf</a>
						<a href=""><i class="fas fa-book"></i> mengenal css width dan height  </a>
						<a href=""><i class="fas fa-book"></i> mengenal html formatif dan kutipan</a>
						<a href=""><i class="fas fa-book"></i> mengenal css text dan font</a>
						<a href=""><i class="fas fa-play-circle"></i> mengenal html komentar dan warna</a>
						<a href=""><i class="fas fa-book"></i> mengenal Css border, margin , dan.... </a>
				
						<a href="../Learning/isilearningquisperbedaanpadding.php"><i class="fas fa-star"></i> apa perbedaan antara padding dan.... </a>
							
						<a href=""><i class="fas fa-book"></i> mengenal html image dan html table</a>
						<a href=""><i class="fas fa-play-circle"></i> mengenal html form </a>
						<a href=""><i class="fas fa-book"></i> mengenal css float dan overflow</a>
						<a href=""><i class="fas fa-book"></i> mengenal html layout </a>
							
						<a href=""><i class="fas fa-play-circle"></i> mengenal css opacity dan navigation bar</a>
						<a href="../Learning/isilearningpengertiancanvas.php"><i class="fas fa-book"></i> mengenal html canvas dan map </a>
						<a href="../Learning/isilearningpengertiandropdown.php"><i class="fas fa-play-circle"></i> mengenal css dropdown dan media query</a>
						<a href="../tubes.php"><i class="fas fa-trophy"></i> Buatlah sebuah website sesuai tema .... </a>
					</div>
				</div>
				<div class="h">
				<?php 
					include("../config.php");
					$data=$connect->query("select * from learning where id_learning=1");
					while($row=$data->fetch_object()){
						$random=$row->judulquis;
					?>
						<h1><?php  echo $row->judulquis;  ?> </h1>
						
						<div class="tess">
							<form style="margin-left:15%;" action="" method="GET">
							<?php 
							
							$username=$_SESSION['username'];
								$tissu=$connect->query("select * from member where username='$username'");
								$bulan=$tissu->fetch_object();
							?>
							<input type="hidden" name="username" value="   <?php echo $bulan->username; ?> " > 
							<input type="hidden" name="id_learning" value="   <?php echo $row->id_learning; ?> " > 
								<p> <?php echo $row ->soal_pertama; ?></p>
							
									<input  type="radio" name="jawaban_satu"  value="Padding-left"><i style="font-size:16px;font-family:Calibri Light ;color:#212121;">padding-left</i><br>
									<input type="radio" name="jawaban_satu" value="Margin-left"><i style="font-size:16px;font-family:Calibri Light ;color:#212121;">Margin-left</i><br>
									<input type="radio" name="jawaban_satu" value="Width"><i style="font-size:16px;font-family:Calibri Light ;color:#212121;">Width</i><br>
									<input type="radio" name="jawaban_satu" value="Height"> <i style="font-size:16px;font-family:Calibri Light ;color:#212121;">Height</i><br>
								
								<p> <?php  echo $row->soal_kedua; ?> </p><br>
									<textarea  style="height:21%;"name="jawaban_dua" placeholder="masukkan jawaban"> </textarea>
								<p> <?php echo $row ->soal_tiga; ?> </p><br>
									<textarea style="height:10%;" name="jawaban_tiga" placeholder="masukkan jawaban"> </textarea><br>
								<p> <?php echo $row ->soal_empat; ?> </p><br>
									<textarea style="height:24%" name="jawaban_empat" placeholder="masukkan jawaban"> </textarea><br><br>
									<input type="submit" name="quispadding" value="Sumbit" style=" border-radius:5px; width:80%; height:5%; background:#37474F; color:#ffff; ">
							</form>
						</div>
				<?php 
					}
				?>
				
				</div>
				<?php 
				if(isset($_GET['quispadding'])){
					$username=$_GET['username'];
					$id_learning=$_GET['id_learning'];
					$judulquis=$random;
					$jawaban_satu=$_GET['jawaban_satu'];
					$jawaban_dua=$_GET['jawaban_dua'];
					$jawaban_tiga=$_GET['jawaban_tiga'];
					$jawaban_empat=$_GET['jawaban_empat'];	
				 $output=$connect->query
				 ("insert into mulai_quis (jawaban_satu,jawaban_dua,jawaban_tiga,jawaban_empat,username,judulquis,id_learning) values ('$jawaban_satu','$jawaban_dua','$jawaban_tiga','$jawaban_empat','$username','$judulquis','$id_learning')");
				 if($output){
					 echo "<script> alert('Berhasil di Inputkan!');</script>";

				 }
				 else{
					  echo "<script> alert('Gagal Menginput Data!');</script>";
				 }
				}				 
				?>
				
			</div>		
		</section>
		<footer>
			<div class="foote">
				<div class="latar">
					<div class="logoo">
						<div class="deskripsi">
							<h1>Koda Coach</h1>
						</div>
						<div class="deskripsii">
							<p>
							 sebuah aplikasi berbasis web yang memuiliki tujuan sebagai pelepor pendidikan di indonesia khususnya dalam bidang program. disusun dengan modul modul terpercaya yang dapat mempermudah proses pembelajaran.
							</p>
						</div>
					</div>
					<div class="logoa">
							<div class="samp">
							<a href="../Learning/learninglogin.php"><i class="fas fa-graduation-cap"></i>Learning</a>
							</div>
							<div class="samp">
							<a href="../Events/eventslogin.php"><i class="far fa-calendar-alt"></i>Events</a>
							</div>
							<div class="samp">
							<a href="../Challenge/challengeloggin.php"><i class="fas fa-trophy"></i>Challenge</a>
							</div>
							<div class="samp">
							<a href="../aboutus/aboutuslogin.php"><i class="fas fa-user"></i>About Us</a>
							</div>
					</div>
					<div class="logon">
						<div class="clas">
							<div class="bund">
								<i class="fab fa-facebook-f"></i>
							</div>
							<div class="bunder">
								<i class="fab fa-twitter"></i>
							</div>
							<div class="bunder">
								<i class="fab fa-telegram-plane"></i>
							</div>
							<div class="bunder">
								<i class="fab fa-google-plus-g"></i>
							</div>
						</div>
						<div class="clak">
							<img src="../img/li.png">
							<img src="../img/b.png">
							<img src="../img/c.png">
							<img src="../img/f.png">
						</div>
					</div>
				</div>
				<div class="latarr">
				</div>
			</div>
		</footer>	
	</body>
</html>