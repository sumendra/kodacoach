<!DOCTYPE html>
<html>
<head>
	<!-- Bootstrap -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!--- CSS -->
	<link rel="stylesheet" type="text/css" href="style/reward.css">


	<title>Reward | Koda Coach</title>
</head>
<body>
	<header>
	<?php  
			include("connect.php");
			session_start();
	?>
			<div class="hover">
				<div class="toggle">
					<div class="logi"><a href="indexlogin.php"><img src="img/k.png"></a></div>
					<i class="fas fa-bars menu"></i>	
				</div>
				<ul>
					<div class="logo"><a href="indexlogin.php"><img src="img/k.png"></a></div>
					  <li><a href="Learning/learninglogin.php"><i class="fas fa-graduation-cap"></i>Learning</a></li>
					  <li><a href="Events/eventslogin.php"><i class="far fa-calendar-alt"></i></i>Events</a></li>
					  <li><a href="Challenge/challengelogin.php"><i class="fas fa-trophy"></i>Challenge</a></li>
					  <li><a href="reward.php"><i class="fas fa-gift"></i>Reward</a></li>
				
				<div class="login">
					  <div class="pete" style="margin-top:8px;float:left;width:40px;height:40px;" ><a href="Profil/profil.php"><img src="img/user.png" style="display:block;width:auto;height:100%;margin-top:5px;margin-left:-20px;  "></a></div>
				</div>
			</div>
			</ul>
		</header>
			<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
				<script type="text/javascript">
					$(document).ready(function(){
						$('.menu').click(function(){
						$('ul').toggleClass('active');
						})
					})
			</script>
<div class="container">

	<div class="row">
    <div class="col-3">
      <h2 style="font-size: 18pt;"> 
      	<b>REWARDS</b>
      	<br><br>
      </h2>
      <p style="font-size: 12pt; color: #34495E;">
      	Akumulasi point dari Event, Quiz, maupun Challenge dapat ditukarkan dengan reward produktif yang dapat digunakan untuk mengembangkan produk teknologi lebih lanjut.
      </p>
      	
    </div>
    <div class="col-8">
    		<div class="filter">
    			Filter by :
    			<button type="button" class="button-1" style="font-size: 10pt">A - Z</button>
    			<button type="button" class="button-1" style="font-size: 10pt">Point Tertinggi</button>
    		</div>
			<?php  
			$username=$_SESSION['username'];
			$jumlah_poin=mysqli_query($connect,"select total_poin from member where username='$username' ");
			$koneksi=mysqli_fetch_array($jumlah_poin);
			?>
    		<br>
    		<p style="color: #CF000F">
    			My Points : <?php 
echo $koneksi['total_poin'];
				?> POIN
    		</p>
		
		<?php 
			$pintu=mysqli_query($connect,"select * from reward where id_reward=1 or id_reward=2 or id_reward=3 or id_reward=4 or id_reward=5 ");
			while($bersih=mysqli_fetch_array($pintu)){
		?>
    <div class="border">
    	<div class="item-1">
    		 <div  class="item-img">
    		 	<img src="images/<?php  echo $bersih['gambar_reward']; ?>" width="200px">
    		 </div>
    		 	<div class="item-information">
    		 		<h4><a href="" style="color: #CF000F; font-size: 16pt;"><b><?php  echo $bersih['judul_reward'];  ?></b></a></h4>
    		 		<p style="font-size: 11pt;">
    		 			<?php  echo $bersih['deskripsi_reward'];  ?>
    		 		</p>
    		 	<p class="point">
    		 		<a href="#what-is-points" data-toggle="modal" class="blue"><small><?php echo $bersih['poin'];  ?> Pts</small></a>
    		 	</p>
    		 		<div class="filter">
					<?php  
						if($bersih['id_reward']==1){
							$username=$_SESSION['username'];
							$sepatu=mysqli_query($connect,"select * from member where username='$username'");
							$data=mysqli_fetch_array($sepatu);
							$username=$data['username'];
							$jumlah_poin=$data['total_poin'];
					?>
					<form method="POST" action="" >
						<input type="hidden" value="<?php   echo $username ?>" name="username">
						<input type="hidden" name="id_reward" value="<?php echo $bersih['id_reward']; ?>"> 
						<input type="hidden" name="poin" value="<?php echo $bersih['poin'];?>  ">
						<input type="hidden" name="jumlah_poin" value="<?php $jumlah_poin ?>">
    		 			<input type="submit"  style="font-size: 10pt;width:100px; height:37px; background:#37474F; color:white;" name="reward"   value="Ambil Reward" >
					</form>
					<?php   
					}
						if(isset($_POST['reward'])){
							$poin=$_POST['poin'];
							$username=$_POST['username'];
							$id_reward=$_POST['id_reward'];
							$jumlah_poin=$_POST['jumlah_poin'];
							if($jumlah_poin>=$poin){
								$data=mysqli_query($connect,"insert into ambil_reward values ('','$username','$id_reward')");
								if($data){
									$bola=mysqli_query($connect,"update set member total_poin=total_poin-'$poin' where username='$username'");
									if($bola){
										echo "<script>alert('berhasil menginput')</script>" ;
									}
									else {
										echo "<script>alert('gagal mengupdate')</script>";
									}
								}
								else {
									echo "<script>alert('gagal input')</script>";
								}
							}
							else {
								echo"<script>alert('poin anda kurang')</script>";
							}
							}
					?>
					<?php  
						if($bersih['id_reward']==2){
							$username=$_SESSION['username'];
							$sepatu=mysqli_query($connect,"select * from member where username='$username'");
							$data=mysqli_fetch_array($sepatu);
							$username=$data['username'];
							$jumlah_poin=$data['total_poin'];
					?>
					<form method="POST" action="" >
						<input type="hidden" value="<?php   echo $username ?>" name="username">
						<input type="hidden" name="id_reward" value="<?php echo $bersih['id_reward']; ?>"> 
						<input type="hidden" name="poin" value="<?php echo $bersih['poin'];?>  ">
						<input type="hidden" name="jumlah_poin" value="<?php $jumlah_poin ?>">
    		 			<input type="submit"  style="font-size: 10pt;width:100px; height:37px; background:#37474F; color:white;" name="reward"   value="Ambil Reward" >
					</form>
					<?php   
					}
						if(isset($_POST['reward'])){
							$poin=$_POST['poin'];
							$username=$_POST['username'];
							$id_reward=$_POST['id_reward'];
							$jumlah_poin=$_POST['jumlah_poin'];
							if($jumlah_poin>=$poin){
								$data=mysqli_query($connect,"insert into ambil_reward values ('','$username','$id_reward')");
								if($data){
									$bola=mysqli_query($connect,"update set member total_poin=total_poin-'$poin' where username='$username'");
									if($bola){
										echo "<script>alert('berhasil menginput')</script>" ;
									}
									else {
										echo "<script>alert('gagal mengupdate')</script>";
									}
								}
								else {
									echo "<script>alert('gagal input')</script>";
								}
							}
							else {
								echo"<script>alert('poin anda kurang')</script>";
							}
							}
					?>
					<?php  
						if($bersih['id_reward']==3){
							$username=$_SESSION['username'];
							$sepatu=mysqli_query($connect,"select * from member where username='$username'");
							$data=mysqli_fetch_array($sepatu);
							$username=$data['username'];
							$jumlah_poin=$data['total_poin'];
					?>
					<form method="POST" action="" >
						<input type="hidden" value="<?php   echo $username ?>" name="username">
						<input type="hidden" name="id_reward" value="<?php echo $bersih['id_reward']; ?>"> 
						<input type="hidden" name="poin" value="<?php echo $bersih['poin'];?>  ">
						<input type="hidden" name="jumlah_poin" value="<?php $jumlah_poin ?>">
    		 			<input type="submit"  style="font-size: 10pt;width:100px; height:37px; background:#37474F; color:white;" name="reward"   value="Ambil Reward" >
					</form>
					<?php   
					}
						if(isset($_POST['reward'])){
							$poin=$_POST['poin'];
							$username=$_POST['username'];
							$id_reward=$_POST['id_reward'];
							$jumlah_poin=$_POST['jumlah_poin'];
							if($jumlah_poin>=$poin){
								$data=mysqli_query($connect,"insert into ambil_reward values ('','$username','$id_reward')");
								if($data){
									$bola=mysqli_query($connect,"update set member total_poin=total_poin-'$poin' where username='$username'");
									if($bola){
										echo "<script>alert('berhasil menginput')</script>" ;
									}
									else {
										echo "<script>alert('gagal mengupdate')</script>";
									}
								}
								else {
									echo "<script>alert('gagal input')</script>";
								}
							}
							else {
								echo"<script>alert('poin anda kurang')</script>";
							}
							}
					?>
					<?php  
						if($bersih['id_reward']==4){
							$username=$_SESSION['username'];
							$sepatu=mysqli_query($connect,"select * from member where username='$username'");
							$data=mysqli_fetch_array($sepatu);
							$username=$data['username'];
							$jumlah_poin=$data['total_poin'];
					?>
					<form method="POST" action="" >
						<input type="hidden" value="<?php   echo $username ?>" name="username">
						<input type="hidden" name="id_reward" value="<?php echo $bersih['id_reward']; ?>"> 
						<input type="hidden" name="poin" value="<?php echo $bersih['poin'];?>  ">
						<input type="hidden" name="jumlah_poin" value="<?php $jumlah_poin ?>">
    		 			<input type="submit"  style="font-size: 10pt;width:100px; height:37px; background:#37474F; color:white;" name="reward"   value="Ambil Reward" >
					</form>
					<?php   
					}
						if(isset($_POST['reward'])){
							$poin=$_POST['poin'];
							$username=$_POST['username'];
							$id_reward=$_POST['id_reward'];
							$jumlah_poin=$_POST['jumlah_poin'];
							if($jumlah_poin>=$poin){
								$data=mysqli_query($connect,"insert into ambil_reward values ('','$username','$id_reward')");
								if($data){
									$bola=mysqli_query($connect,"update set member total_poin=total_poin-'$poin' where username='$username'");
									if($bola){
										echo "<script>alert('berhasil menginput')</script>" ;
									}
									else {
										echo "<script>alert('gagal mengupdate')</script>";
									}
								}
								else {
									echo "<script>alert('gagal input')</script>";
								}
							}
							else {
								echo"<script>alert('poin anda kurang')</script>";
							}
							}
					?>
					<?php  
						if($bersih['id_reward']==5){
							$username=$_SESSION['username'];
							$sepatu=mysqli_query($connect,"select * from member where username='$username'");
							$data=mysqli_fetch_array($sepatu);
							$username=$data['username'];
							$jumlah_poin=$data['total_poin'];
					?>
					<form method="POST" action="" >
						<input type="hidden" value="<?php   echo $username ?>" name="username">
						<input type="hidden" name="id_reward" value="<?php echo $bersih['id_reward']; ?>"> 
						<input type="hidden" name="poin" value="<?php echo $bersih['poin'];?>  ">
						<input type="hidden" name="jumlah_poin" value="<?php $jumlah_poin ?>">
    		 			<input type="submit"  style="font-size: 10pt;width:100px; height:37px; background:#37474F; color:white;" name="reward"   value="Ambil Reward" >
					</form>
					<?php   
					}
						if(isset($_POST['reward'])){
							$poin=$_POST['poin'];
							$username=$_POST['username'];
							$id_reward=$_POST['id_reward'];
							$jumlah_poin=$_POST['jumlah_poin'];
							if($jumlah_poin>=$poin){
								$data=mysqli_query($connect,"insert into ambil_reward values ('','$username','$id_reward')");
								if($data){
									$bola=mysqli_query($connect,"update set member total_poin=total_poin-'$poin' where username='$username'");
									if($bola){
										echo "<script>alert('berhasil menginput')</script>" ;
									}
									else {
										echo "<script>alert('gagal mengupdate')</script>";
									}
								}
								else {
									echo "<script>alert('gagal input')</script>";
								}
							}
							else {
								echo"<script>alert('poin anda kurang')</script>";
							}
							}
					?>
    		 		</div>
    		 	</div>
    		 	
    	</div>
   	  </div> <!-- End Border -->
		<?php  
			}
		?>
<ul class="pagination">
    <li class="page-item">
      <a class="page-link" href="#" aria-label="Previous">
        <span aria-hidden="true">&laquo;</span>
        <span class="sr-only">Previous</span>
      </a>
    </li>
    <li class="page-item"><a class="page-link" href="#">1</a></li>
    <li class="page-item"><a class="page-link" href="#">2</a></li>
    <li class="page-item"><a class="page-link" href="#">3</a></li>
    <li class="page-item">
      <a class="page-link" href="#" aria-label="Next">
        <span aria-hidden="true">&raquo;</span>
        <span class="sr-only">Next</span>
      </a>
    </li>
</ul>

	 </div> <!-- end col-8 -->
	</div> <!-- end row -->
</div><!-- end Container -->



<footer style="background-color: #ddd;width: 100%;">
      <div class="container" style="margin-top: 0px;">
        <div class="row">
          <div class="col-md-4">
            <span class="copyright">Copyright © Koda Coach 2018</span>
          </div>
          <div class="col-md-4">
            <ul class="list-inline social-buttons">
              <li class="list-inline-item">
                <a href="../challenge/challenge.html">
                  <i class="fa fa-twitter"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="../challenge/challenge.html">
                  <i class="fa fa-facebook"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="../challenge/challenge.html">
                  <i class="fa fa-linkedin"></i>
                </a>
              </li>
            </ul>
          </div>
          <div class="col-md-4">
            <ul class="list-inline quicklinks">
              <li class="list-inline-item">
                <a href="../challenge/challenge.html">Privacy Policy</a>
              </li>
              <li class="list-inline-item">
                <a href="../challenge/challenge.html">Terms of Use</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </footer>











	<!-- javascript -->
	 <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	
</body>
</html>