<?php
	
	session_start();
	if(isset($_SESSION['id_user'])){
		header("location:./isiadmin.php");
		die();
	}
	include("connect.php");

 ?>

<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="style/loginadmin.css">
	<link rel="stylesheet" type="text/css" href="login.css">

	<!-- bootsrtap -->
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
		<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
	<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>


	<title>
		Login Admin | Koda Coach
	</title>
</head>
<body>
<?php 
	if( isset( $_POST['login'] ) ){

        //mendeklarasikan data yang akan dimasukkan ke dalam database
		$username = $_POST['username'];
		$password = $_POST['password'];

        //skript query ke insert data ke dalam database
		$sql = mysqli_query($connect, "SELECT id_user, username, nama, alamat,hp FROM user WHERE username='$username' AND password=MD5('$password')");

        //jika skript query benar maka akan membuat session
		if( $sql){
			list($id_user, $username, $nama,$alamat, $hp ) = mysqli_fetch_array($sql);

            //membuat session
            $_SESSION['id_user'] = $id_user;
			$_SESSION['username'] = $username;
			$_SESSION['nama'] = $nama;
			$_SESSION['alamat'] = $alamat;
			$_SESSION['hp'] = $hp;
			header("Location: ./isiadmin.php");
			die();
		} else {

			$_SESSION['err'] = '<strong>ERROR!</strong> Username dan Password tidak ditemukan.';
			header('Location: ./loginadmin.php');
			die();
		}
	}
	else {


?>

	<section class="login-block">
    <div class="container">
	<div class="row">
		<div class="col-md-4 login-sec">
		    <h2 class="text-center">Login-Admin.</h2>
		    <form class="login-form" method="POST" action=" ">
			<?php 
			if(isset($_SESSION['err'])){
				$err=$_SESSION['err'];
				echo '<div class="alert alert-danger" role="alert">
  						'.$err.'	
					</div>';
				unset ($_SESSION['err']);
			}
			?>
  <div class="form-group">
    <label for="exampleInputEmail1" class="text-uppercase">Username</label>
    <input type="text" class="form-control" name="username" placeholder="">
    
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1" class="text-uppercase">Password</label>
    <input type="password" class="form-control" name="password" placeholder="">
  </div>
  
  
    <div class="form-check">
    <label class="form-check-label">
      <input type="checkbox" class="form-check-input">
      <small>Remember Me</small>
    </label>
    <button type="submit" class="btn btn-login float-right" name="login">Login as Admin</button>
	<div class="role" style="width:20px; height:20px; background:;">
	<a style="text-decoration:none;" href="../even/login.php">Kembali</a>
	</div>
  </div>
  
</form>
	<?php 
	
			}
	
	?>
<div class="copy-text">Created By </i><a href="./admin/pages/index.php">Koda Coach.</a></div>
		</div>
		<div class="col-md-8 banner-sec">
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                 <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                  </ol>
            <div class="carousel-inner" role="listbox">
    <div class="carousel-item active">
      <img class="d-block img-fluid" src="https://static.pexels.com/photos/33972/pexels-photo.jpg" alt="First slide">
      <div class="carousel-caption d-none d-md-block">
        <div class="banner-text">
            <h2>This is Koda Coach</h2>
            <p>Koda Coach adalah sebuah Platform Education yang berfokus kepada <b>"Programming & Design"</b> yang memberikan <i>Experience</i> yang berbeda dalam mempelajari Programming</p>
        </div>	
  </div>
    </div>
    <div class="carousel-item">
      <img class="d-block img-fluid" src="https://images.pexels.com/photos/7097/people-coffee-tea-meeting.jpg" alt="First slide">
      <div class="carousel-caption d-none d-md-block">
        <div class="banner-text">
            <h2>This is " K. " Corporation</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation</p>
        </div>	
    </div>
    </div>
    <div class="carousel-item">
      <img class="d-block img-fluid" src="https://static.pexels.com/photos/497848/pexels-photo-497848.jpeg" alt="First slide">
      <div class="carousel-caption d-none d-md-block">
        <div class="banner-text">
            <h2>This is Heaven</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation</p>
        </div>	
    </div>
  </div>
            </div>	   
		    
		</div>
	</div>
</div>
</section>



</body>
</html>