<?php   session_start(); ?>
<html>
	<head>
		<title>

		</title>
		<link rel="stylesheet" href="style/style.css"> 
		<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
		<link rel="stylesheet" href="style/responsife.css">
		<link rel="stylesheet" href="style/indexlogin.css"> 
	</head>
	<style>
		* {box-sizing: border-box}
		body {font-family:Calibri Light; sans-serif; margin:0}
		.mySlides {display: none}
		img {vertical-align: middle;}

		.slideshow-container {
		  max-width: 1800px;
		  position: relative;
		  margin: auto;
		}
		.prev, .next {
		  cursor: pointer;
		  position: absolute;
		  top: 50%;
		  width: auto;
		  padding: 16px;
		  margin-top: -22px;
		  color: white;
		  font-weight: bold;
		  font-size: 18px;
		  transition: 0.6s ease;
		  border-radius: 0 3px 3px 0;
		}
		.next {
		  right: 0;
		  border-radius: 3px 0 0 3px;
		}
		.prev:hover, .next:hover {
		  background-color: rgba(0,0,0,0.8);
		}
		.tais {
		  color: #f2f2f2;
		  font-size:40px;
		  padding: 0px 12px;
		  position: absolute;
		  top:500px;
		  bottom: 8px;
		  width: 100%;
		  text-align: center;
		}
		.text {
		  color: #f2f2f2;
		  font-size:30px;
		  padding: 8px 12px;
		  position: absolute;
		  bottom: 8px;
		  width: 100%;
		  text-align: center;
		}
		.numbertext {
		  color: #f2f2f2;
		  font-size: 40px;
		  padding: 36%;  100px;
		  position: absolute;
		  top: 0;
		}
		.dot {
		  cursor: pointer;
		  height: 15px;
		  width: 15px;
		  margin: 0 2px;
		  background-color: #bbb;
		  border-radius: 50%;
		  display: inline-block;
		  transition: background-color 0.6s ease;
		}

		.active, .dot:hover {
		  background-color: #717171;
		}
		.fade {
		  -webkit-animation-name: fade;
		  -webkit-animation-duration: 1.5s;
		  animation-name: fade;
		  animation-duration: 1.5s;
		}

		@-webkit-keyframes fade {
		  from {opacity: .4} 
		  to {opacity: 1}
		}

		@keyframes fade {
		  from {opacity: .4} 
		  to {opacity: 1}
		}
		@media screen and (max-width: 960px) {
		.tais {
		  top:400px;
		}
		}
		@media only screen and (max-width: 600px) {
		  .prev, .next,.text {font-size: 20px}
		}
		}
		@media screen and (max-width: 860px) {
		.tais {
		  font-size:30px;
		}
		.text {
		  font-size:20px;
		}
		}
		@media screen and (max-width: 770px) {
		.tais {
		  top:200px;
		}
		}
		@media screen and (max-width: 770px) {
		.tais {
		  font-size:24px;
		}
		.text {
		  font-size:14px;
		}
		}
		@media screen and (max-width: 480px) {
		.tais {
		  font-size:24px;
		}
		.text {
		  font-size:14px;
		}
		}
		@media screen and (max-width: 400px) {
		.tais {
		  top:460px;
		}
		}
		@media screen and (max-width: 360px) {
		.tais {
		  top:500px;
		}
		}
		@media screen and (max-width: 320px) {
		.tais {
		  top:400px;
		}
		}
		</style>
	<body>
		<header>
			<div class="hover">
				<div class="toggle">
					<div class="logi"><a href="indexlogin.php"><img src="img/k.png"></a></div>
					<i class="fas fa-bars menu"></i>	
				</div>
				<ul>
					<div class="logo"><a href="indexlogin.php"><img src="img/k.png"></a></div>
					  <li><a href="Learning/learninglogin.php"><i class="fas fa-graduation-cap"></i>Learning</a></li>
					  <li><a href="Events/eventslogin.php"><i class="far fa-calendar-alt"></i></i>Events</a></li>
					  <li class="dropdown">
						<a href="" class="dropbtn"><i class="fas fa-trophy"></i>Challenge</a>
						<div class="dropdown-content">
						  <a href="Challenge/challengelogin.php">Masih Berlangsung</a>
						  <a href="#">Sudah Selesai</a>
						  <a href="#">Sedang Berlangsung</a>
						</div>
					  </li>
					  <li><a href="reward.php"><i class="fas fa-gift"></i>Reward</a></li>
				
				<div style="width:60px;height:60px;" class="login">
					  <div style="margin-top:8px;width:120px;height:100%;" class="pete"><a href="Profil/profil.php"><img style="width:auto;height:60%; " src="img/user.png"></a></div>
				</div>
				</ul>
			</div>
			
		</header>
			<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
				<script type="text/javascript">
					$(document).ready(function(){
						$('.menu').click(function(){
						$('ul').toggleClass('active');
						})
					})
			</script>
		<section>
				<div style="margin-top:4px;" class="slideshow-container">
				<div style="width:100%;"class="mySlides fade">
				  <img src="images/k.jpg" style="height:92%;width:100%;">
				  <div class="text">Belajar Langsung Dari Ahlinya</div>
				</div>

				<div class="mySlides fade">
				  <img src="images/buku.jpg" style="width:100%; height:94%;">
				   <div class="tais">Raih Keunginanmu Bersama Kami</div>
				  <div class="text">Belajar Program dan Desaign dari Develover kelas Dunia</div>
				</div>

				<div class="mySlides fade">
				  <div class="numbertext"></div>
				  <img src="images/c.jpg" style="width:100%;height:92%;">
				   <div class="tais">Lihat Bakat Yang Kamu Miliki</div>
				  <div class="text">Kembangkan Bakaymu dan bekerja di Industri yang kalian Inginkan </div>
				</div>

				<a class="prev" onclick="plusSlides(-1)">&#10094;</a>
				<a class="next" onclick="plusSlides(1)">&#10095;</a>

				</div>
				<br>
				<div style="text-align:center">
				  <span class="dot" onclick="currentSlide(1)"></span> 
				  <span class="dot" onclick="currentSlide(2)"></span> 
				  <span class="dot" onclick="currentSlide(3)"></span> 
				</div>
				<script>
				var slideIndex = 1;
				showSlides(slideIndex);

				function plusSlides(n) {
				  showSlides(slideIndex += n);
				}

				function currentSlide(n) {
				  showSlides(slideIndex = n);
				}

				function showSlides(n) {
				  var i;
				  var slides = document.getElementsByClassName("mySlides");
				  var dots = document.getElementsByClassName("dot");
				  if (n > slides.length) {slideIndex = 1}    
				  if (n < 1) {slideIndex = slides.length}
				  for (i = 0; i < slides.length; i++) {
					  slides[i].style.display = "none";  
				  }
				  for (i = 0; i < dots.length; i++) {
					  dots[i].className = dots[i].className.replace(" active", "");
				  }
				  slides[slideIndex-1].style.display = "block";  
				  dots[slideIndex-1].className += " active";
				}
				</script>
		</section>
		<section>
			<div style="margin-top:7.9px;" class="contain">
				<div class="selesih">
				</div>
				<div class="texts">
					<h2>LEARNING</h2>
				</div>
				<div class="conten">
				<?php
				include("connect.php");
					$query=$connect->query(" select * from learning where id_learning=1 or id_learning=2 or id_learning=3  ");
						if($query->num_rows!=0){
							While($data=$query->fetch_object()){
				?>
				
					<div class="webPemula">
						<div class="tulisan">
							<h2><?php echo $data->judullearning ?> </h2>
						</div>
						<div class="isian">
							<div class="image"> 
								<img src="img/<?php echo $data-> image_learning ?>">
							</div>
							<div class="write">
								<p><?php echo $data->deskripsi ?></p>
								<div class="user">
									<p><i class="fas fa-user"></i><?php echo $data-> jumlah_siswa ?></p>
									<p><i class="fas fa-gift"></i><?php echo $data-> xp;echo"xp &nbsp"; echo $data->poin;echo"poin &nbsp"; ?></p>
									<p><i class="fas fa-chess-queen"></i><?php echo $data-> tingkat_kelas ?></p>
									<p><i class="fas fa-unlock-alt"></i><?php echo $data ->status?></p>
								</div>
									<?php 
										if($data->id_learning==1){
											
												$username=$_SESSION['username'];
												$like=$connect->query("select * from member where username ='$username' ");
												$tampung=$like->fetch_object();
												$id=$tampung->id_member;
												$username=$tampung->username;
										?>
													 <form action="Learning/isilearningwebbeginner.php" method="POST">
													<input type="hidden" name="waktu" value="<?php echo date("y/m/d");?>">
													<?php
														echo'
														<input type="hidden" name="poin" value="'.$data->poin.'">
														<input type="hidden" name="xp" value="'.$data->xp.'">
														<input type="hidden" name="username" value="'.$username.'">
														<input type="hidden" name="id_learning" value="'.$data->id_learning.'">
														<input type="hidden" name="id_member" value="'.$id.'">
														<input style="width:90%; height:18%; background:#37474F;color:white; margin-top:2%;" type="submit" name="submit" value="Mulai">
													</form>';	
													?>
										<?php
										}
										else if ($data->id_learning==2){
											echo'<form action=" " method="POST">
												<input style="width:90%; height:18%; background:#37474F;color:white; margin-top:2%;" type="submit" name="submit" value="Mulai">
											</form>';
										}
										else if($data->id_learning==3){
											echo'<form action=" " method="POST">
												<input style="width:90%; height:18%; background:#37474F;color:white; margin-top:2%;" type="submit" name="submit" value="Mulai">
											</form>';
										}
									?>			
							</div>
						</div>
					</div>
					<?php 
							}
						}
					
					?>	
				</div>
			</div>
		</section>
		<section>
			<div class="containn">
				<div  style="background:white;" class="selesih">
				</div>
				<div class="textss">
					<h2 style=
					"padding-left:41px;">EVENTS</h2>
				</div>
				<div class="contenn">
					<?php
						include("config.php");
						$query=$connect->query(" select * from events where id_events=1 or id_events=2 or id_events=3  ");
							if($query->num_rows!=0){
								While($data=$query->fetch_object()){
					?>
					<div class="network">
						<div class="tulisan">
							<h2> <?php echo $data->judulevents ?> </h2>
						</div>
						<div class="isiann">
							<div class="image"> 
								<img src="img/<?php echo $data-> gambar_events ?>">
							</div>
							<div class="write">
								<p> <?php echo $data->deskripsi ?></p>
								<div class="user">
									<p><i class="fas fa-user"></i> <?php echo $data->jumlah_siswa ?> </p>
									<p><i class="fas fa-gift"></i> <?php echo $data-> xp;echo"xp &nbsp"; echo $data->poin;echo"poin &nbsp"; ?>  </p>
									<p><i class="fas fa-chess-queen"></i> <?php echo $data->status ?> </p>
									<p><i class="fas fa-map-marker"></i><?php echo $data->lokasi ?></p>
								</div>
									<?php
										if($data->id_events==1){
												$username=$_SESSION['username'];
												$like=$connect->query("select * from member where username ='$username' ");
												$tampung=$like->fetch_object();
												$id=$tampung->id_member;
									?>
													<form action=" " method="POST">
														<input type="hidden" name="waktu" value="<?php echo date("y/m/d");?>">
									<?php 
													echo' 
														<input type="hidden" name="id_events" value="'.$data->id_events.'">
														<input type="hidden" name="id_member" value="'.$id.'">
														<input type="hidden" name="poin" value="'.$data->poin.'">
														<input type="hidden" name="" value="'.$data->xp.'">
														<input style="width:90%; height:18%; background:#37474F;color:white; margin-top:2%;" type="submit" name="submit" value="Mulai">
													</form>';
										}
										else if ($data->id_events==2){
											echo'<form action=" " method="POST">
												<input style="width:90%; height:18%; background:#37474F;color:white; margin-top:2%;" type="submit" name="submit" value="Mulai">
												</form>';
										}
										else if($data->id_events==3){
											$username=$_SESSION['username'];
												$like=$connect->query("select * from member where username ='$username' ");
												$id_member=$_SESSION['id_member'];
												$tampung=$like->fetch_object();
												$id=$tampung->id_member;
												$username=$tampung->username;
												$jumlahpoin=$tampung->total_poin;
									?>
													<form action=" Events/isievent.php" method="POST">
														<input type="hidden" name="waktu" value="<?php echo date("y/m/d");?>">
									<?php
													echo'
														<input type="hidden" name="id_events" value="'.$data->id_events.'">
														<input type="hidden" name="id_member" value="'.$id.'">
														<input type="hidden" name="harga" value="'.$data->harga.'">
														<input type="hidden" name="poin" value="'.$data->poin.'">
														<input type="hidden" name="xp" value="'.$data->xp.'">
														<input type="hidden" name="username" value="'.$username.'">
														<input type="hidden" name="jumlah_poin" value="'.$jumlahpoin.'">
														<input style="width:90%; height:18%; background:#37474F;color:white; margin-top:2%;" type="submit" name="submit" value="Mulai">
													</form>';
										}
									?>		
							</div>
						</div>
					</div>
					<?php 
						}
					}
					?>
				</div>
			</div>
		</section>
		<section>
		<div class="contain">
		<div class="selesih">
		</div>
				<div class="textt">
					<h2>CHALLENGE</h2>
				</div>
				<div class="conten">
				<?php
				include("connect.php");
					$query=$connect->query(" select * from challengel where id_challenge=1 or id_challenge=2 or id_challenge=3  ");
						if($query->num_rows!=0){
							While($data=$query->fetch_object()){
				?>
					<div class="webPemula">
						<div class="tulisan">
							<h2> <?php echo $data->judulchallenge ?> </h2>
						</div>
						<div class="isian">
							<div class="image"> 
								<img src="img/<?php echo $data->gambar_challenge ?> ">
							</div>
							<div class="write">
								<p> <?php echo $data->deskripsi ?> </p>
								<div class="user">
									<p><i class="fas fa-user"></i> <?php echo $data->jumlah_siswa ?> </p>
									<p><i class="fas fa-gift"></i> <?php  echo $data-> xp;echo"xp &nbsp"; echo $data->poin;echo"poin &nbsp"; ?> </p>
									<p><i class="fas fa-chess-queen"></i> <?php echo $data->status ?> </p>
									<p><i class="fas fa-chess-queen"></i> <?php echo $data->tingkat_kelas ?> </p>
									<p><i class="fas fa-map-marker"></i> <?php echo $data->lokasi ?> </p>
								</div>
								<?php 
										if($data->id_challenge==2){
											
												$username=$_SESSION['username'];
												$like=$connect->query("select * from member where username ='$username' ");
												$tampung=$like->fetch_object();
												$id=$tampung->id_member;
												$username=$tampung->username;
												$jumlahpoin=$tampung->total_poin;
										?>
													 <form action="Challenge/isichallenge.php" method="POST">
														<input type="hidden" name="waktu" value="<?php echo date("y/m/d");?>">
														<?php
														echo'
														<input type="hidden" name="id_challenge" value="'.$data->id_challenge.'">
														<input type="hidden" name="id_member" value="'.$id.'">
														<input type="hidden" name="harga" value="'.$data->harga.'">
														<input type="hidden" name="poin" value="'.$data->poin.'">
														<input type="hidden" name="xp" value="'.$data->xp.'">
														<input type="hidden" name="username" value="'.$username.'">
														<input type="hidden" name="jumlah_poin" value="'.$jumlahpoin.'">
														<input style="width:90%; height:18%; background:#37474F;color:white; margin-top:2%;" type="submit" name="submit" value="Mulai">
													</form>';
														?>
										<?php
										
										}
										else if ($data->id_challenge==1){
											echo'<form action=" " method="POST">
												<input style="width:90%; height:18%; background:#37474F;color:white; margin-top:2%;" type="submit" name="submit" value="Mulai">
											</form>';
										}
										else if($data->id_challenge==3){
											echo'<form action=" " method="POST">
												<input style="width:90%; height:18%; background:#37474F;color:white; margin-top:2%;" type="submit" name="submit" value="Mulai">
											</form>';
										}
									?>			
							</div>
						</div>
					</div>
					<?php
						}
					}
					?>
				</div>
			</div>
		</section>
		<section>
			<div class="testi">
			<div style="background:white;" class="selesih">
			</div>
				<div class="texti">
				<h2>Keunggulan </h2>
				</div>
				<div class="testim">
					<div class="empat">
						<img src="img/g.png">
						<p>terdapat sistem sertifikat dan  poin pada Koda coach yang nantinya dapat di tukar dengan fitur Koda coach</p>
					</div>
					<div class="lima">
						<img src="img/im.png">
						<p>kumpulkan sebanyak banyaknya  xp dari learning dan events untuk dapat menjadi bagian dari Koda coach</p>
					</div>
					<div class="enam">
						<img src="img/fi.png">
						<p>terdapat banyak modul modul yang dapat menunjang dan mempermudah dan mempercepat pembelajaran  </p>
					</div>
					<div class="tujuh">
						<img src="img/le.png">
						<p>terdapat banyak seminar yang dibuat untuk mempercepat dan mempermudah pelajaran kalian </p>
					</div>
				</div>
			</div>
		</section>
		<section>
			<div class="bagian">
			<div class="selesih" >
				</div>
				<div class="wada">
					<div class="delapan">
						<div class="d">
						</div>
						<div class="la">
							<div class="e">
							</div>
							<div class="le">
								<p>Poncut Ridha</p>
							</div>
						</div>
						<div class="l">
							<p>Belajar di Koda Coach menurut saya sangat mudah. karena modul modulnya sudah di design sesederhana mungkin. jadi cocoklah buat pemula.Pokoknya the best deh buat Koda Coach</p>
						</div>
					</div>
					<div class="sembilan">
						<div class="m">
						</div>
						<div class="la">
							<div class="e">
							</div>
							<div class="le">
								<p>Danis Zaedans</p>
							</div>
						</div>
						<div class="l">
							<p>Sebelum belajar di sini saya kira belajar pemrograman itu susah tapi, setelah belajar di sini saya merasa program tidaklah terlalu sulit</p>
						</div>
					</div>
					<div class="sepuluh">
						<div class="s">
						</div>
						<div class="la">
							<div class="e">
							</div>
							<div class="le">
								<p>Yoga Setiawan</p>
							</div>
						</div>
						<div class="l">
							<p>Modul modulnya bagus banget terasa kuliah beneran Koda Coach memang best lecture</p>
						</div>
					</div>
					<div class="sepuluhnext">
						<div class="p">
						</div>
						<div class="la">
							<div class="e">
							</div>
							<div class="le">
								<p>Wanda Widayanti</p>
							</div>
						</div>
						<div class="l">
							<p>Ngak nyangka ada juga website indonesia yang punya modul selengkap ini. biasanya aku belajar di web luar. walaupun ngerti tapi ngak maksimal juga. makasih Koda Coach</p>
						</div>
					</div>
				</div>
			</div>
		</section>
		<footer>
			<div class="foote">
				<div class="latar">
					<div class="logoo">
						<div class="deskripsi">
							<h1>Koda Coach</h1>
						</div>
						<div class="deskripsii">
							<p>
							 sebuah aplikasi berbasis web yang memuiliki tujuan sebagai pelepor pendidikan di indonesia khususnya dalam bidang program. disusun dengan modul modul terpercaya yang dapat mempermudah proses pembelajaran.
							</p>
						</div>
					</div>
					<div class="logoa">
							<div class="samp">
							<a href="Learning/learninglogin.php"><i class="fas fa-graduation-cap"></i>Learning</a>
							</div>
							<div class="samp">
							<a href="Events/eventslogin.php"><i class="far fa-calendar-alt"></i>Events</a>
							</div>
							<div class="samp">
							<a href="Challenge/challengelogin.php"><i class="fas fa-trophy"></i>Challenge</a>
							</div>
							<div class="samp">
							<a href="aboutus/aboutuslogin.php"><i class="fas fa-user"></i>About Us</a>
							</div>
					</div>
					<div class="logon">
						<div class="clas">
							<div class="bund">
								<i class="fab fa-facebook-f"></i>
							</div>
							<div class="bunder">
								<i class="fab fa-twitter"></i>
							</div>
							<div class="bunder">
								<i class="fab fa-telegram-plane"></i>
							</div>
							<div class="bunder">
								<i class="fab fa-google-plus-g"></i>
							</div>
						</div>
						<div class="clak">
							<img src="img/li.png">
							<img src="img/b.png">
							<img src="img/c.png">
							<img src="img/f.png">
						</div>
					</div>
				</div>
				<div class="latarr">
				</div>
			</div>
		</footer>	
	</body>
</html>