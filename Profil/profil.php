<?php 
session_start();
		include("../connect.php");
 ?>

<html>
	<head>
		<title>
			Profile | Koda Coach
		</title>
			<link rel="stylesheet" href="../style/style.css"> 
			<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
			<link rel="stylesheet" href="../style/responsife.css">
			<link rel="stylesheet" href="../style/events.css"> 
			<link rel="stylesheet" href="../style/learning.css"> 
			<link rel="stylesheet" href="../style/profil.css"> 
			<link rel="stylesheet" href="../style/responsifeprofil.css"> 
			<style>
				body {font-family:Calibri Light , sans-serif; margin:0}
			</style>

	<!-- BOOTSTRAP -->

	</head>
	<body>
		<header>
			<div class="hover">
				<div class="toggle">
					<div class="logi"><a href="../indexlogin.php"><img src="../img/k.png"></a></div>
					<i class="fas fa-bars menu"></i>	
				</div>
				<ul>
				  <div class="logo"><a href="../indexlogin.php"><img src="../img/k.png"></a></div>
				  <li><a href="../Learning/learning.php"><i class="fas fa-graduation-cap"></i>Learning</a></li>
				  <li><a href="../Events/events.php"><i class="far fa-calendar-alt"></i></i>Events</a></li>
				  <li class="dropdown">
					<a href="javascript:void(0)" class="dropbtn"><i class="fas fa-trophy"></i>Challenge</a>
					<div class="dropdown-content">
					  <a href="../Challenge/challenge.php">Masih Berlangsung</a>
					  <a href="#">Sudah Selesai</a>
					  <a href="#">Sedang Berlangsung</a>
					</div>
				  </li>
				<li><a href="../reward.php"><i class="fas fa-gift"></i>Reward</a></li>
				  
				<div class="login">
					<form method="POST" action="">
					<?php
					$username=$_SESSION['username'];
					$total_poin=mysqli_query ($connect,"select * from member where username='$username' ");
					while($point=mysqli_fetch_array($total_poin)){
					?>
					<input type="hidden" name="poin_events" value="<?php echo $point['poin_events'];?>">
					<input type="hidden" name="poin_challenge" value="<?php echo $point['poin_challenge'];?>">
					<input type="hidden" name="poin_learning" value="<?php echo $point['poin_learning'];?>">
					<input type="hidden" name="xp_learning" value="<?php echo $point['xp_learning'];?>">
					<input type="hidden" name="xp_events" value="<?php echo $point['xp_events'];?>">
					<input type="hidden" name="xp_challenge" value="<?php echo $point['xp_challenge'];?>">
					<input style="width:100px;height:40px;background:#37474F;color:white; margin-top:10px; border-radius:6px;" type="submit" name="subm" value="keluar">
					<?php  	
					}
					?>	
					 </form>
						<?php  
						if(isset($_POST['subm'])){
							$poin_learning=$_POST['poin_learning'];
							$poin_challenge=$_POST['poin_challenge'];
							$poin_events=$_POST['poin_events'];
							$xp_learning=$_POST['xp_learning'];
							$xp_events=$_POST['xp_events'];
							$xp_events=$_POST['xp_events'];
							$processor=mysqli_query($connect ,"update member set total_poin=0+'$poin_events'+'$poin_challenge'+'$poin_learning' ,   total_xp=0+'$xp_learning'+'$xp_events'+'$xp_challenge' where username='$username' ");
							if($processor){
								header("location:../index.php");
							}
						}
					

					?>
				</div>
			</div>
			</ul>
		</header>
			<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
				<script type="text/javascript">
					$(document).ready(function(){
						$('.menu').click(function(){
						$('ul').toggleClass('active');
						})
					})
				</script>
		<?php 
			$username=$_SESSION['username'];
			$query= $connect-> query ( " SELECT * from member where username ='$username' ");
				if($query->num_rows!=0){
					while ($dewa=$query->fetch_object()){
		?>
		<section>
			<div class="container">
				<div class="content" >
					<div class="selisih">
					</div>
					<div class="text">
						<img src="profil/uploads/<?php echo $dewa->avatar_nama; ?>">
						<center>
						<p style="padding-top:1px;color:white;font-size:27px;"> <?php echo $dewa-> username; echo $dewa->id_member;?> </p>
						<p  style="font-size:16px;padding:0;;color:white;"> <?php  echo $dewa->waktu;?> </p>
						<p style="font-size:16px;padding:0;;color:white;"> <?php  echo $dewa->total_xp;echo"xp &nbsp"; echo $dewa->total_poin ;echo"poin &nbsp"; ?> </p>
						</center>
					</div>
				</div>
			</div>
		</section>
		<button class="tablink" onclick="openPage('Home', this, '#78909C')">Tentang Saya</button>
			<button class="tablink" onclick="openPage('News', this, '#78909C')" id="defaultOpen">Learning</button>
			<button class="tablink" onclick="openPage('Contact', this, '#78909C')">Events</button>
			<button class="tablink" onclick="openPage('About', this, '#78909C')">Challenge</button>
		<div id="Home" class="tabcontent">

				 <h2>Biodata</h2>
				<p style="padding-top:2%;">Nama &emsp; &nbsp; &nbsp; &nbsp; &nbsp; :  &nbsp; <?php echo $dewa->username ?> </p>
				<p>Email &emsp; &nbsp; &nbsp; &nbsp; &nbsp;  : &nbsp; <?php echo $dewa->email; ?> </p>
				<p>Asal &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  &nbsp; &nbsp; &nbsp;&nbsp;   : &nbsp; <?php echo $dewa->asal; ?> </p>
				<p>No Telpon &nbsp; &nbsp;&nbsp;&nbsp;:&nbsp; <?php echo $dewa->telepon; ?> </p><br>
				
					<a style="color:white;" href="updateprofile.php?id_member=<?php echo $dewa->id_member;?>">
						<button style="border: none;border-radius: 4px;padding: 8px 16px; cursor: pointer;">
							Edit Profile
						</button>
					</a>

<!-- ID HOME for TENTANG SAYA -->
		</div>
					<?php 
					}
					}
						$username=$_SESSION['username'];
							$query= mysqli_query($connect, "select m.id_mulai, m.id_member,m.id_learning,me.username,l.deskripsi,l.image_learning,l.jumlah_siswa,l.tingkat_kelas,l.xp,l.poin,l.harga,l.status, l.judullearning from mulai_learning m inner join member me on m.id_member=me.id_member inner join learning l on m.id_learning=l.id_learning where me.username='$username' ");
						while($data_learn=mysqli_fetch_array($query)){
					?>
				<div id="News" class="tabcontent">
					<center><h2>LEARNING</h2></center>
				<div class="ex3">
				<div class="conten" style="height:auto;">
					<div class="webPemula" style="height:100%;margin-top:0;">
						<div class="bagi">
							<div class="class">
								<div class="danusa">
									<p><?php echo $data_learn['judullearning'];?></p>
								</div>
								<div class="danu">
									<div class="imga">
										<img src="../img/<?php echo $data_learn['image_learning'];?>">
									</div>
									<div class="texi">
										<p><?php echo $data_learn['deskripsi'];?></p>
									</div>
								</div>
							</div>
							<div class="cla">
								
							</div>
							<div class="cl">
								<div class="danusa">
									<p>Fitur Fitur</p>
								</div>
								<div class="danu">
									<div class="texu">
										<p href=""><i class="fas fa-user"></i><?php echo $data_learn['jumlah_siswa'];?></p>
										<p href=""><i class="fas fa-gift"></i><?php  echo $data_learn['xp'];echo"xp &nbsp"; echo $data_learn['poin'];echo"poin &nbsp"; ?> </p>
										<p href=""><i class="fas fa-chess-queen"> </i><?php echo $data_learn['tingkat_kelas']; ?> </p>
										<p href=""><i class="fas fa-unlock-alt"></i> <?php echo $data_learn['status'];?></p>
										<p href=""><i class="fas fa-dollar-sign"></i><?php echo $data_learn['harga']; ?></p>
									</div>
								</div>
							</div>
							</div>
						<div class="bagian">
						<li>
							<a>Mulai</a>
							</li>
						</div>					
					</div>
				</div>
				</div>
				</div>


				<?php 
					}
				?>
				<?php 
							$username=$_SESSION['username'];
							$query= mysqli_query($connect, "select m.id_mulai, m.id_member,m.id_events,me.username,e.deskripsi,e.gambar_events,e.jumlah_siswa,e.tingkat_kelas,e.xp,e.harga,e.poin,e.status, e.judulevents from mulai_events m inner join member me on m.id_member=me.id_member inner join events e on m.id_events=e.id_events where me.username='$username' ");
						while($siwa=mysqli_fetch_array($query)){
				?>
				<div id="Contact" class="tabcontent">
				  <center><h2>Events</h2></center>
				<div class="ex3">
				<div class="conten" style="height:auto;">
					<div class="webPemula" style="height:100%;margin-top:0;">
						<div class="bagi">
							<div class="class">
								<div class="danusa">
									<p><?php echo $siwa['judulevents'];?></p>
								</div>
								<div class="danu">
									<div class="imga">
										<img src="../img/<?php echo $siwa['gambar_events'];?>">
									</div>
									<div class="texi">
										<p><?php echo $siwa['deskripsi']?></p>
									</div>
								</div>
							</div>
							<div class="cla">
								
							</div>
							<div class="cl">
								<div class="danusa">
									<p>Fitur Fitur</p>
								</div>
								<div class="danu">
									<div class="texu">
										<p href=""><i class="fas fa-user"></i><?php echo $siwa['jumlah_siswa']?></p>
										<p href=""><i class="fas fa-gift"></i><?php  echo $siwa['xp'];echo"xp &nbsp"; echo $siwa['poin'];echo"poin &nbsp"; ?> </p>
										<p href=""><i class="fas fa-chess-queen"> </i><?php echo $siwa['tingkat_kelas']; ?> </p>
										<p href=""><i class="fas fa-unlock-alt"></i> <?php echo $siwa['status'];?></p>
										<p href=""><i class="fas fa-dollar-sign"></i><?php echo $siwa['harga']; ?></p>
									</div>
								</div>
							</div>
							</div>
						<div class="bagian">
						<li>
							<a>Mulai</a>
							</li>
						</div>					
					</div>
				</div>
				</div>
				</div>
				<?php 
				
					}
				
				?>
				<?php 
							$username=$_SESSION['username'];
							$query= mysqli_query($connect, "select m.id_mulai, m.id_member , m.id_challenge, me.username, c.deskripsi, c.gambar_challenge, c.jumlah_siswa, c.tingkat_kelas, c.xp, c.status,c.poin, c.lokasi,  c.judulchallenge from mulai_challenge m inner join member me on m.id_member=me.id_member inner join challengel c on m.id_challenge=c.id_challenge where me.username='$username' ");
						while($data_challenge=mysqli_fetch_array($query)){
				?>
				<div id="About" class="tabcontent">
					<center><h2>Challenge</h2></center>
				<div class="ex3">
				<div class="conten" style="height:auto;">
					<div class="webPemula" style="height:100%;margin-top:0;">
						<div class="bagi">
							<div class="class">
								<div class="danusa">
									<p><?php echo $data_challenge['judulchallenge'];?></p>
								</div>
								<div class="danu">
									<div class="imga">
										<img src="../img/<?php echo $data_challenge['gambar_challenge'];?>">
									</div>
									<div class="texi">
										<p><?php echo $data_challenge['deskripsi'];?></p>
									</div>
								</div>
							</div>
							<div class="cla">
								
							</div>
							<div class="cl">
								<div class="danusa">
									<p>Fitur Fitur</p>
								</div>
								<div class="danu">
									<div class="texu">
										<p href=""><i class="fas fa-user"></i><?php echo $data_challenge['jumlah_siswa'];?></p>
										<p href=""><i class="fas fa-gift"></i><?php echo $data_challenge['xp'];echo"xp &nbsp"; echo $data_challenge['poin'];echo"poin &nbsp"; ?> </p>
										<p href=""><i class="fas fa-chess-queen"> </i><?php echo $data_challenge['tingkat_kelas']; ?> </p>
										<p href=""><i class="fas fa-unlock-alt"></i> <?php echo $data_challenge['status'];?></p>
										<p><i class="fas fa-map-marker"></i> <?php echo $data_challenge['lokasi']; ?> </p>
									</div>
								</div>
							</div>
							</div>
						<div class="bagian">
						<li>
							<a>Mulai</a>
							</li>
						</div>					
					</div>
				</div>
			</div>
		</div>
		<?php 
			}
		?>
		<script>
			function openPage(pageName,elmnt,color) {
				var i, tabcontent, tablinks;
				tabcontent = document.getElementsByClassName("tabcontent");
					for (i = 0; i < tabcontent.length; i++) {
						tabcontent[i].style.display = "none";
					}
					tablinks = document.getElementsByClassName("tablink");
					for (i = 0; i < tablinks.length; i++) {
						tablinks[i].style.backgroundColor = "";
					}
					document.getElementById(pageName).style.display = "block";
					elmnt.style.backgroundColor = color;
				}
				document.getElementById("defaultOpen").click();
			</script>
		<footer>
			<div class="foote">
				<div class="latar">
					<div class="logoo">
						<div class="deskripsi">
							<h1>Koda Coach</h1>
						</div>
						<div class="deskripsii">
							<p>
							 sebuah aplikasi berbasis web yang memuiliki tujuan sebagai pelepor pendidikan di indonesia 
							 khususnya dalam bidang program. disusun dengan modul modul terpercaya yang dapat mempermudah 
							 proses pembelajaran.
							</p>
						</div>
					</div>
					<div class="logoa">
							<div class="samp">
							<a href="../Learning/learning.php"><i class="fas fa-graduation-cap"></i>Learning</a>
							</div>
							<div class="samp">
							<a href="../Events/events.php"><i class="far fa-calendar-alt"></i>Events</a>
							</div>
							<div class="samp">
							<a href="../Challenge/challenge.php"><i class="fas fa-trophy"></i>Challenge</a>
							</div>
							<div class="samp">
							<a href="../aboutus/aboutuslogin.php"><i class="fas fa-user"></i>About Us</a>
							</div>
					</div>
					<div class="logon">
						<div class="clas">
							<div class="bund">
								<i class="fab fa-facebook-f"></i>
							</div>
							<div class="bunder">
								<i class="fab fa-twitter"></i>
							</div>
							<div class="bunder">
								<i class="fab fa-telegram-plane"></i>
							</div>
							<div class="bunder">
								<i class="fab fa-google-plus-g"></i>
							</div>
						</div>
						<div class="clak">
							<img src="../img/li.png">
							<img src="../img/b.png">
							<img src="../img/c.png">
							<img src="../img/f.png">
						</div>
					</div>
				</div>
				<div class="latarr">
				</div>
			</div>
		</footer>	
	</body>
</html>