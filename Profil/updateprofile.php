<?php 
    include("../connect.php");

  $id_member= $_GET['id_member'];
  $query = mysqli_query($connect,"SELECT * FROM member WHERE id_member='$id_member'");
  while($data = mysqli_fetch_array($query)){


 ?>




<!DOCTYPE html>
<html lang="en">

<head>
    <link rel="stylesheet" type="text/css" href="./style/css.css">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha256-3dkvEK0WLHRJ7/Csr0BZjAWxERc5WH7bdeUya2aXxdU= sha512-+L4yy6FRcDGbXJ9mPG8MT/3UCDzwR9gPeyFNMCtInsol++5m3bk2bXWKdZjvybmohrAsn3Ua5x8gfLnbE1YkOg==" crossorigin="anonymous">

    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<!--     <link href="css/bootstrap.min.css" rel="stylesheet"> -->
      

    <title>Edit Profile | Koda Cocah</title>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
  

   <div class="container">
<div class="row">
<div class="col-md-10 ">


  <!-- UPLOAD IMAGE -->
<?php 
  $id_member= $_GET['id_member'];
  if (isset($_POST['submit'])) {
      $filename = $_FILES['upload']['name'];
      $filetmpname = $_FILES['upload']['tmp_name'];
      $folder = 'uploads/';

      move_uploaded_file($filetmpname, $folder.$filename);

     

  $query = $connect->query("UPDATE member SET avatar = '$filename', avatar_nama = '$filename' WHERE id_member = '$id_member'");
  if($query){
	   echo "<div class='alert alert-success'>Berhasil Mengubah Profile</div>";
	  header("location:profil.php");
  }
  elseif ($filename = null) {
  echo "tidak mengganti avatar";
}
}
if (!$query) {
    
  echo "<div class='alert alert-danger'>Gagal Mengubah Profile</div>";
  }

 ?>

<form class="form-horizontal" method="post" enctype="multipart/form-data">

<fieldset>

<!-- Form Name -->
<legend>User profile</legend>

<!-- Text input-->

<div class="form-group">
  <label class="col-md-4 control-label" for="Name (Full name)">Nama (Username)</label>  
  <div class="col-md-4">
 <div class="input-group">
       <div class="input-group-addon">
        <i class="fa fa-user">
        </i>
       </div>
       <input id="Name" name="username" type="text" value="<?php echo $data['username'];?>" class="form-control input-md">
      </div>

    
  </div>

  
</div>

<!-- File Button --> 
<div class="form-group">
  <label class="col-md-4 control-label" for="Upload photo">Upload Photo</label>
  <div class="col-md-4">
    <input id="Upload photo" name="upload" class="input-file" type="file">
  </div>
</div>

<!-- Text input-->





<div class="form-group">
  <label class="col-md-4 control-label col-xs-12" for="Address">Alamat</label>  
 <!--  <div class="col-md-4  col-xs-4">
  <input id="Address" name="Address" type="text" placeholder="Provinsi" class="form-control input-md ">
  </div> -->


  <div class="col-md-4 col-xs-4">

  <input id="Address" name="asal" type="text" placeholder="Kota" value="<?php echo $data['asal'];?>" class="form-control input-md ">
  </div>

  
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="Phone number ">Nomor Hand Phone </label>  
  <div class="col-md-4">
  
      <div class="input-group othertop">
       <div class="input-group-addon">
     <i class="fa fa-mobile fa-1x" style="font-size: 20px;"></i>
        
       </div>
    <input id="Phone number " name="telepon" type="text" value=" <?php echo $data['telepon'];?>" class="form-control input-md">
    
      </div>
  
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="Email Address">Email</label>  
  <div class="col-md-4">
  <div class="input-group">
       <div class="input-group-addon">
     <i class="fa fa-envelope-o"></i>
        
       </div>
    <input name="email" type="email" class="form-control input-md" value=" <?php echo $data['email'];?>">
    
      </div>
  
  </div>
</div>


<!-- Textarea -->
<div class="form-group">
  <label class="col-md-4 control-label" for="Overview (max 200 words)">Status</label>
  <div class="col-md-4">                     
    <textarea class="form-control" rows="10" placeholder="Deskripsi (max 200 words)"  id="Overview (max 200 words)" name="Deskripsi"></textarea>
  </div>
</div>


<div class="form-group">
  <label class="col-md-4 control-label" ></label>  
  <div class="col-md-4">
    <button class="btn btn-success" type="Submit" name="submit"><span class="glyphicon glyphicon-thumbs-up"></span> Submit
    </button>
  <button class="btn btn-danger" type="reset"><span class="glyphicon glyphicon-remove-sign"></span> Clear</button>
  
    
  </div>
</div>

<a href="profil.php">cancel</a>

</fieldset>
</form>
</div>
<div class="col-md-2 hidden-xs">
<img src="../profil/uploads/<?php echo $data['avatar_nama'] ?>" class="img-responsive img-thumbnail">
  </div>


</div>
   </div>
    <!-- jQuery Version 1.11.1 -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>




<?php
  }
          if(isset($_POST['submit'])){
            $username=$_POST['username'];
            $email=$_POST['email'];
            $asal=$_POST['asal'];
            $telepon=$_POST['telepon'];
            
            $input= mysqli_query($connect,"UPDATE member set username='$username',email='$email', asal='$asal',telepon='$telepon' where id_member='$id_member'");
              
          }
    
      
    ?>