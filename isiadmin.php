<?php 
session_start();
if(empty($_SESSION['id_user'])){
	$_SESSION['err']='Anda harus login terlebih dahulu ';
	header('location:./loginadmin.php');
	die();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Admin | Koda Coach</title>
    <link href="admin/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="admin/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
    <link href="admin/dist/css/sb-admin-2.css" rel="stylesheet">
    <link href="admin/vendor/morrisjs/morris.css" rel="stylesheet">
    <link href="admin/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
</head>
<body>

	
    <div id="wrapper">
        <nav class="navbar navbar-inverse navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php" style="color: white;">Koda Coach</a>
            </div>
			
            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-envelope fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-messages">
					<?php include("connect.php");
				
					$bela=mysqli_query ($connect, "select * from user ");
					while($rama=mysqli_fetch_array($bela)){?>
                       
                        <li class="divider"></li>
                        <li>
                            <a href=" ">
                                <div>
                                    <strong><?php echo $rama['username'];?></strong>
                                    <span class="pull-right text-muted">
                                        <em>17 m yang lalu</em>
                                    </span>
                                </div>
                                <div><?php echo $rama['keterangan'];?></div>
                            </a>
                        </li>
					<?php } ?>
                    </ul>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-tasks fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-tasks">
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <p>
                                        <strong>Learing</strong>
                                        <span class="pull-right text-muted">80% Complete</span>
                                    </p>
                                    <div class="progress progress-striped active">
                                        <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%">
                                            <span class="sr-only">80% Complete (danger)</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <p>
                                        <strong>Event</strong>
                                        <span class="pull-right text-muted">20% Complete</span>
                                    </p>
                                    <div class="progress progress-striped active">
                                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                                            <span class="sr-only">20% Complete</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
                                    <p>
                                        <strong>Challenge</strong>
                                        <span class="pull-right text-muted">60% Complete</span>
                                    </p>
                                    <div class="progress progress-striped active">
                                        <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
                                            <span class="sr-only">60% Complete (warning)</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>
                        
                        <li class="divider"></li>
                        <li>
                            <a class="text-center" href="#">
                                <strong>See All Tasks</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                    </ul>
                    <!-- /.dropdown-tasks -->
                </li>
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-bell fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-alerts">
                        <li class="divider"></li>
                        <li>
                            <a href="#">
                                <div>
								<?php $news=mysqli_query($connect,"select waktu,max(id_member) as id_member from member order by id_member asc"); while($a=mysqli_fetch_array($news)){?>
                                    <i class="fa fa-twitter fa-fw"></i><?php echo $a['id_member'];?> Member
								<?php } ?>
								<?php  
									$rama= mysqli_query ($connect ,  "select waktu from member where id_member=(select max(id_member) from member)"); 
									$siwa=mysqli_fetch_array($rama);
									$waktu=$siwa['waktu'];
									include 'time_since.php';
									$lalu=time_since(strtotime ($waktu) );
								?>
                                    <span class="pull-right text-muted small"><?php echo $lalu ?></span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a class="text-center" href="#">
                                <strong>See All Alerts</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="logout.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                            </div>
                        <li>
                            <a href=""><i class="fas fa-columns"></i> Dashboard</a>
                        </li>
						<li>
                            <a  href="tables.php"><i style="padding-right:6px;" class="fas fa-user"></i></i> Table Member<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
								<li>
                                    <a href="tables.php">Member</a>
                                </li>
                                <li>
                                    <a href="tablelearning.php">Learning</a>
                                </li>
                                <li>
                                   <a href="tableevents.php">Events</a>
                                </li>
                                <li>
                                    <a href="tablechallenge.php">Challenge</a>
                                </li>
								 <li>
                                    <a href="tablequis.php">Quis</a>
                                </li>
                                <li>
                                    <a href="tabletubes.php">Tubes</a>
                                </li>
								 <li>
                                    <a href="tablebayarevents.php">Bayar Events</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#"><i style="padding-right:6px;" class="fas fa-book"></i></i> Table Materi<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="tableisilearning.php">Learing</a>
                                </li>
                                <li>
                                    <a href="tableisievents.php">Events</a>
                                </li>
                                <li>
                                    <a href="tableisichallnege.php">Challenge</a>
                                </li>
								 <li>
                                    <a href="tableisiquis.php">Quis</a>
                                </li>
                                <li>
                                    <a href="tableisitubes.php">Tubes</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Dashboard</h1>
                </div>
            <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-comments fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">26</div>
                                    <div>New Comments!</div>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-yellow">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i style="font-size:64px;" class="fas fa-graduation-cap"></i>
                                </div>
                                <div class="col-xs-9 text-right">
								<?php 
								$date=mysqli_query($connect, " select max(id_mulai) as id_mulai from mulai_learning order by id_mulai asc " );
								while($ta_learning=mysqli_fetch_array ($date)){
								?>
                                 <div class="huge"><?php echo $ta_learning['id_mulai'];?></div>
								<?php
								}
								?>
                                   <div>New Learning Orders!</div>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
				
				<div class="col-lg-3 col-md-6">
                    <div class="panel panel-red">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i style="font-size:64px;" class="fas fa-calendar-alt"></i>
                                </div>
                                <div class="col-xs-9 text-right">
								<?php 
								$data=mysqli_query($connect, " select max(id_mulai) as id_mulai from mulai_events order by id_mulai asc " );
								while($ta=mysqli_fetch_array ($data)){
								?>
                                <div class="huge"><?php echo $ta['id_mulai']; ?></div>
								<?php }?>	
								
								
                                    <div>New Events Orders!</div>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
				<div class="col-lg-3 col-md-6">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i style="font-size:64px;" class="fas fa-trophy"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">124</div>
                                    <div>New Challenge Orders!</div>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-8">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bar-chart-o fa-fw"></i> Member dari Koda Coach
                            <div class="pull-right">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                        Options
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu pull-right" role="menu">
                                        <li><a href="#">Configure</a>
                                        </li>
                                        <li><a href="#">Details</a>
                                        </li>
                                        <li><a href="#">Report Data</a>
                                        </li>
                                        <li class="divider"></li>
                                        <li><a href="#">Copy Data</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    <div class="panel panel-default">
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-4">
                                    
                                        <table class="table table-bordered table-hover table-striped">
                                            <thead>
                                                <tr>
                                                   
                                                    <th>Email</th>
                                                    <th>Username</th>
                                                  
                                                </tr>
                                            </thead>
											<?php 
												$data=mysqli_query($connect, " select * from member  " );
												while($ta=mysqli_fetch_array ($data)){
											?>
                                            <tbody>
                                                <tr>
                                                  
                                                    <th><?php echo $ta['email']; ?></th>
                                                    <th><?php echo $ta['username']; ?></th>
                                                    
                                                </tr>
                                            </tbody>
											<?php 
												}
											?>
                                        </table>
                                    
                                    <!-- /.table-responsive -->
                                </div>
                                <!-- /.col-lg-4 (nested) -->
                                <div class="col-lg-8">
                                    <div id="morris-bar-chart"></div>
                                </div>
                                <!-- /.col-lg-8 (nested) -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-clock-o fa-fw"></i>Timeline Tahap-Tahap Penting !
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <ul class="timeline">
                                <li>
                                    <div class="timeline-badge"><i class="fa fa-check"></i>
                                    </div>
                                    <div class="timeline-panel">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title">Verifikasi</h4>
                                            </p>
                                        </div>
                                        <div class="timeline-body">
                                            <p>Melakukan Verifikasi untuk para Koda Coacher sangatlah penting. Karena kita harus memiliki data lengkap yang telah di Verifikasi oleh user. Gunanya adalah agar data yang mereka miliki akan tetap 'stay' di Profile mereka. Serta pencapaian yang telah mereka lakukan. kita Nggak mau kan user mendapatkan Experience yang buruk di sini ?</p>
                                        </div>
                                    </div>
                                </li>
                                <li class="timeline-inverted">
                                    <div class="timeline-badge warning"><i class="fa fa-credit-card"></i>
                                    </div>
                                    <div class="timeline-panel">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title">Pembayaran</h4>
                                        </div>
                                        <div class="timeline-body">
                                            <p>Nah di tahap ini, kita harus siap siaga dalam melayani pembayaran serta verifikasi atas pembayaran yang telah dilakukan user.</p>
                                            <p>Agar para user langsung dapat menikmati Fitur yang telah ia bayar. Kita ngga mau kan user kita harus menunggu waktu yang lama hanya untuk membayar ?.</p>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="timeline-badge danger"><i class="fa fa-bomb"></i>
                                    </div>
                                    <div class="timeline-panel">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title">Security System</h4>
                                        </div>
                                        <div class="timeline-body">
                                            <p>Dalam waktu sekali seminggu kita akan maintenance untuk memperketat sistem keamanan dan jaringan server pada Koda Coach. Agar user tidak mengalami kecurangan ataupun hack dalam melakukan pembelajaran</p>
                                        </div>
                                    </div>
                                </li>
                                <li class="timeline-inverted">
                                    <div class="timeline-panel">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title">Note</h4>
                                        </div>
                                        <div class="timeline-body">
                                            <p>Bagi Mereka yang terdeteksi dalam melakukan kecurangan, akun nya akan segera di ban ya :)</p>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="timeline-badge info"><i class="fa fa-save"></i>
                                    </div>
                                    <div class="timeline-panel">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title">Sisten Penyimpanan</h4>
                                        </div>
                                        <div class="timeline-body">
                                            <p>Oh iya! kita Memiliki Sistem Penyimpanan yang berupa CLOUD. jadi setiap sekali seminggu harus maintenance ya !</p>
                                            <hr>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="timeline-panel">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title">Sistem Cloud</h4>
                                        </div>
                                        <div class="timeline-body">
                                            <p>Sistem penyimpanan Cloud didapatkan jika user membeli sistem penyimpanan Cloud Okay ?</p>
                                        </div>
                                    </div>
                                </li>
                                <li class="timeline-inverted">
                                    <div class="timeline-badge success"><i class="fa fa-graduation-cap"></i>
                                    </div>
                                    <div class="timeline-panel">
                                        <div class="timeline-heading">
                                            <h4 class="timeline-title">Lorem ipsum dolor</h4>
                                        </div>
                                        <div class="timeline-body">
                                            <p>Bagi user yang telah mencapai keberhasilannya, akan mendapatkan Achivement serta hadiah.. Taklupa juga Sertifikat ya !.</p>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-8 -->
                <div class="col-lg-4">
                    <div class="chat-panel panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-comments fa-fw"></i> Chat
                            <div class="btn-group pull-right">
                                <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-chevron-down"></i>
                                </button>
                            </div>
                        </div>
                        <div class="panel-body" style="height:100px;">
                            <ul class="chat">
                                <li class="left clearfix">
                                    <span class="chat-img pull-left">
                                        <img src="http://placehold.it/50/55C1E7/fff" alt="User Avatar" class="img-circle" />
                                    </span>
                                    <div class="chat-body clearfix">
									<?php 
										$username=$_SESSION['username'];
										$go=mysqli_query ($connect, "select * from user where username='$username' ");
										$ju=mysqli_fetch_array ($go);
									
									?>
                                        <div class="header">
                                            <strong class="primary-font"><?php  echo $ju['username'];?></strong>
                                            <small class="pull-right text-muted">
                                             </small>
                                        </div>
                                        <p id="daftarTodo">
                                            
                                        </p>
                                    </div>
                                </li>
                            </ul>
						</div>
                        <div class="panel-footer">
                                <input type="text" class="form-control" id="todo" placeholder="What needs to be done?">
								  <button name="class" class="btn btn-block btn-secondary text-light mt-2" id="addToDo">Kirim</button>
								 <script src="js/data.json"></script>
								  <script src="js/main.js"></script>
								 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="admin/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="admin/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="admin/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="admin/vendor/raphael/raphael.min.js"></script>
    <script src="admin/vendor/morrisjs/morris.min.js"></script>
    <script src="admin/data/morris-data.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="admin/dist/js/sb-admin-2.js"></script>

</body>

