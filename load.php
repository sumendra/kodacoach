<?php 

require_once 'config.php';

$sql = "SELECT * FROM todos";
$result = $connect->query($sql);

$rows = $result->fetch_all(MYSQLI_ASSOC);

$json = json_encode($rows);

echo $json;