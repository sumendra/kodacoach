<!DOCTYPE html>
<html>
<head>
	<!-- pure CSS -->
	<link rel="stylesheet" type="text/css" href="style/sertifikat.css">
	<link rel="stylesheet" type="text/css" href="style/reward.css">
	<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
	<title>Certificate - Koda Coach</title>

	<!-- the Bootstraps -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	
</head>
<body>


<header>
			<div class="hover">
				<div class="toggle">
					<div class="logi"><a href="indexlogin.php"><img src="img/k.png"></a></div>
					<i class="fas fa-bars menu"></i>	
				</div>
				<ul>
					<div class="logo"><a href="indexlogin.php"><img src="img/k.png"></a></div>
					  <li><a href="Learning/learninglogin.php"><i class="fas fa-graduation-cap"></i>Learning</a></li>
					  <li><a href="Events/eventslogin.php"><i class="far fa-calendar-alt"></i></i>Events</a></li>
					  <li><a href="Challenge/challengelogin.php"><i class="fas fa-trophy"></i>Challenge</a></li>
					  <li><a href="reward.php"><i class="fas fa-gift"></i>Reward</a></li>
				
				<div class="login">
					  <div class="pete" style="margin-top:8px;float:left;width:40px;height:40px;" ><a href="Profil/profil.php"><img src="img/user.png" style="display:block;width:auto;height:100%;margin-top:5px;margin-left:-20px;  "></a></div>
				</div>
			</div>
			</ul>
		</header>
			<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
				<script type="text/javascript">
					$(document).ready(function(){
						$('.menu').click(function(){
						$('ul').toggleClass('active');
						})
					})
			</script>
<div class="container bg-white">
	<div class="row">
  			<div class="col-12 col-sm-9">
  				<br>
			<?php  
			session_start();
			$username=$_SESSION['username'];
				include("connect.php");
				$dewa=mysqli_query($connect,"select * from member where username='$username'");
				$dewi=mysqli_fetch_array($dewa);
			?>
  			<h2 class="heading text-center">Certificate</h2>
  			<p class="text-center">
  				Selamat Kepada anda <?php echo $dewi['username'];?> Karena Telah Berpartisipasi dalam Learning Kami
  			</p>
			<div class="tissu">
				<center><h1 style="padding-top:220px;"> <?php echo $dewi['username'];  ?>  </h1></center>
			</div>
  				<br>
  				<br>
  			<p style="font-family:Calibri Light; font-size:20px;">Bagikan Moment ini bersama teman-teman mu</p>
  				<br>
				<form action="tubes.php">
			<button style="float:left; background:#FF7043;border:none;" class="btn btn-info" type="submit">Kembali</button>
			</form>
			<div class="edit" style="width:100px; height:40px;background:#F5F5F5;float:left;text-align:center; padding-top:6px; border-radius:5px;margin-left:10px;">
			
			<?php
			$id_member=$dewi['id_member'];
				include("connect.php");
				$sql=mysqli_query($connect,"SELECT * FROM mulai_tubes where id_member='$id_member' ");
				if($sql){
					$data=mysqli_fetch_array($sql);
						echo '
							<a href="'.$data['sertifikat'].'">Download</a>
						';
				}
				?>
			</div>
			<br><br>

  			</div>
  		<div class="col-6 col-sm-3 bg-light">
  			<br>
  			<h3 class="text-center">Achivement Learning</h3>
  			<br>
  			Hat Trick (50 Exp)
  			<img src="images/hattrick1.png" width="80">
  			<br>
  			<br>
  			Verfied (10 Exp)
  			<img src="images/verified1.png" width="80">
  			<br>
  			<br>
  			Achiver (25 Exp)
  			<img src="images/achiver1.png" width="80">

  		</div>
	</div>
</div>
<footer style="background-color: #ddd;width: 100%;margin-top:20px;">
    <div class="container" style="margin-top: 0px;">
        <div class="row">
          <div class="col-md-4">
            <span class="copyright">Copyright © Koda Coach 2018</span>
          </div>
          <div class="col-md-4">
            <ul class="list-inline social-buttons">
              <li class="list-inline-item">
                <a href="../challenge/challenge.html">
                  <i class="fa fa-twitter"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="../challenge/challenge.html">
                  <i class="fa fa-facebook"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="../challenge/challenge.html">
                  <i class="fa fa-linkedin"></i>
                </a>
              </li>
            </ul>
          </div>
          <div class="col-md-4">
            <ul class="list-inline quicklinks">
              <li class="list-inline-item">
                <a href="../challenge/challenge.html">Privacy Policy</a>
              </li>
              <li class="list-inline-item">
                <a href="../challenge/challenge.html">Terms of Use</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </footer>
	<!-- others JS bootstrap -->
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>
</html>